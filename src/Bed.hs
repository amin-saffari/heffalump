module Bed where

import Bio.Prelude
import Streaming                            ( Of(..), MonadTrans(..), Stream )
import System.IO                            ( withFile, IOMode(..) )
import Util                                 ( vconcats )

import qualified ByteStream                     as S
import qualified Data.ByteString                as B
import qualified Data.ByteString.Char8          as C
import qualified Data.Vector.Algorithms.Intro   as U ( sort )
import qualified Data.Vector.Unboxed            as U
import qualified Streaming.Prelude              as Q

import Genome ( Variant(..) )

newtype Bed = Bed ( U.Vector (Int32,Int32,Int32) )

readBed :: Maybe FilePath -> [ B.ByteString ] -> IO (Maybe Bed)
readBed  Nothing     _ = return Nothing
readBed (Just fp) refs = fmap Just $ withFile fp ReadMode $ parseBed refs . S.hGetContents

parseBed :: [ B.ByteString ] -> S.ByteStream IO r -> IO Bed
parseBed chrms raw = do
    v :> _ <- vconcats . Q.map parse1 . S.lines' . S.decomp $ raw
    U.sort v
    Bed <$> U.unsafeFreeze v
  where
    parse1 :: C.ByteString -> U.Vector (Int32, Int32, Int32)
    parse1 ln = maybe U.empty U.singleton $ do
                   sq:frm:tho:_ <- Just $ C.words ln
                   ci <- findIndex ((==) sq) chrms
                   (start,"") <- C.readInt frm
                   (end,"") <- C.readInt tho
                   return (fromIntegral ci, fromIntegral start, fromIntegral end)

filterWithBed :: Monad m => Maybe Bed -> Stream (Of Variant) m r -> Stream (Of Variant) m r
filterWithBed  Nothing         = id
filterWithBed (Just (Bed vau)) = go (U.toList vau)
  where
    go [                 ] = lift . Q.effects >=> pure
    go ((ch, ps, pe) : rs) = lift . Q.next >=> \case
        Left    r    -> pure r
        Right (v,vs)
            -- variant before interval, drop the variant
            | (v_chr v, v_pos v) <  (fromIntegral ch, fromIntegral ps) -> go ((ch,ps,pe):rs) vs

            -- variant after interval, drop the interval
            | (v_chr v, v_pos v) >= (fromIntegral ch, fromIntegral pe) -> go rs (Q.cons v vs)

            -- must be a good variant
            | otherwise                                                -> v `Q.cons` go ((ch,ps,pe):rs) vs

