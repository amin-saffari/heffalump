module ShameMS
        ( mainShameMSout, blockToShame
        , physicalBlocks, MaxPhysical(..)
        , observedBlocks, MinObserved(..)
        ) where

import Bio.Prelude
import System.Console.GetOpt
import Streaming ( Stream, Of(..), lift, yields, mapsM_ )

import qualified Data.ByteString.Builder         as B
import qualified Data.Vector                     as W
import qualified Data.Vector.Storable            as U
import qualified Streaming.Prelude               as Q

import Bed ( readBed, filterWithBed )
import Genome
import Lump
import Util ( parseFile1Opts, streamConcat, singlesOnly, vconcats )

data ConfShameMS = ConfShameMS {
    conf_noutgroups      :: Maybe Int,
    conf_blocklength     :: Maybe Int,
    conf_min_observed    :: Maybe Int,
    conf_all             :: Bool,
    conf_split           :: Bool,
    conf_reference       :: Maybe FilePath,
    conf_regions         :: Maybe FilePath }

defaultMsConf :: ConfShameMS
defaultMsConf = ConfShameMS (Just 0) Nothing Nothing True True Nothing Nothing

optsVcfout :: [ OptDescr (ConfShameMS -> IO ConfShameMS) ]
optsVcfout =
    [ Option "r" ["reference"]          (ReqArg set_ref "FILE") "Read reference from FILE (.2bit)"
    , Option "n" ["numoutgroups"]       (ReqArg set_nout "NUM") "The first NUM individuals are outgroups (0)"
    , Option "l" ["block-length"]     (ReqArg set_lblock "LEN") "Blocks of maximum length LEN"
    , Option "m" ["num-observed"]     (ReqArg set_tblock "NUM") "Number of observed sites per block"
    , Option "D" ["dense"]                    (NoArg set_dense) "Output invariant sites, too"
    , Option "t" ["only-transversions"]      (NoArg set_no_all) "Output only transversion sites"
    , Option "b" ["only-biallelic"]        (NoArg set_no_split) "Discard, don't split, polyallelic sites"
    , Option "R" ["regions"]           (ReqArg set_rgns "FILE") "Restrict to regions in bed-file FILE" ]
  where
    set_ref    a c = return $ c { conf_reference       =  Just a }
    set_nout   a c = (\n ->   c { conf_noutgroups      =  Just n }) <$> readIO a
    set_lblock a c = (\n ->   c { conf_blocklength     =  Just n }) <$> readIO a
    set_tblock a c = (\n ->   c { conf_min_observed    =  Just n }) <$> readIO a
    set_dense    c = return $ c { conf_noutgroups      = Nothing }
    set_no_all   c = return $ c { conf_all             =   False }
    set_no_split c = return $ c { conf_split           =   False }
    set_rgns   a c = return $ c { conf_regions         =  Just a }

mainShameMSout :: [String] -> IO ()
mainShameMSout args = do
    ( hefs, ConfShameMS{..} ) <- parseFile1Opts defaultMsConf "pseudo_ms"
                                                "[hef-file...]" optsVcfout args

    decodeManyRef conf_reference hefs $ \refs inps -> do
        region_filter <- readBed conf_regions (rss_chroms refs)

        mapsM_ ( maybe id (\l -> (hPutStrLn stdout ("\n//\nblockSize_" ++ show l) >>)) conf_blocklength
               . blockToShame stdout
               . filterWithBed region_filter
               . bool singlesOnly streamConcat conf_split
               . maybe expandVarsDense expandVars conf_noutgroups ) $
            do_blocks conf_min_observed conf_blocklength $
            addRef (MaxHole 49) refs $
            mergeLumps (fromMaybe 0 conf_noutgroups) inps
  where
    do_blocks        _   Nothing  = yields
    do_blocks  Nothing  (Just  l) = physicalBlocks l
    do_blocks (Just  m) (Just  l) = observedBlocks (MinObserved m) (MaxPhysical l)


blockToShame :: Handle -> Stream (Of Variant) IO r -> IO r
blockToShame h = Q.next >=> oneBlock
  where
    oneBlock (Left       r) = return r
    oneBlock (Right (v1,s)) = do
        let stride = U.length (v_calls v1)
        ff :> r <- Q._first (fmap (Block stride) . U.unsafeFreeze) =<< vconcats (Q.map smashVariants (Q.cons v1 s))
        unless (nullBlock ff) $
            B.hPutBuilder h $ foldMap
                (\ind -> oneLine fstW ind <> oneLine sndW ind)
                (individuals ff)
        return r

    oneLine :: (TwoNucs -> Nuc2b) -> U.Vector TwoNucs -> B.Builder
    oneLine which = U.foldr ((<>) . B.char7 . toRefCode . which) (B.char7 '\n')

    smashVariants :: Variant -> U.Vector TwoNucs
    smashVariants Variant{..} =
        U.map (smashVariant v_ref v_alt) v_calls

    smashVariant :: Nuc2b -> Var2b -> AlleleCounts -> TwoNucs
    smashVariant r a = \case
        AC 0 0 -> TwoNucs n n
        AC _ 0 -> TwoNucs r r
        AC 0 _ -> TwoNucs v v
        AC _ _ -> TwoNucs r v
      where
        n = N2b 15
        v = appVar r a


-- | A pair of nucleotides with compact storage.
data TwoNucs = TwoNucs { fstW :: !Nuc2b, sndW :: !Nuc2b }

instance Storable TwoNucs where
    sizeOf _ = 1
    alignment _ = 1
    poke p (TwoNucs (N2b a) (N2b b)) = poke (castPtr p) (a .|. shiftL b 4)
    peek p = (\x -> TwoNucs (N2b $ x .&. 0xF) (N2b $ shiftR x 4 .&. 0xF)) <$> peek (castPtr p)

-- | Creates blocks of constant physical size.  A 'Hole' or a switch to
-- a different target scaffold cause a short block to be emitted and a
-- new one to be started.

physicalBlocks :: Monad m => Int -> Stream (Of Clump) m r -> Stream (Stream (Of Clump) m) m r
physicalBlocks ln = lift . Q.next >=> \case

        Left  r              -> pure r
        Right (Hole{..}, vs) -> physicalBlocks ln vs
        Right (v,        vs) -> yields (span_before (c_chr v) (c_pos v + ln) $ Q.cons v vs) >>= physicalBlocks ln

  where
    span_before :: Monad m => Int -> Int -> Stream (Of Clump) m r -> Stream (Of Clump) m (Stream (Of Clump) m r)
    span_before c p = lift . Q.next >=> \case

        Left r                             -> pure (pure r)

        Right (Hole{..}, vs)               -> pure vs

        Right (v@Variants{}, vs)
            | c_chr v /= c || c_pos v >= p -> pure (v `Q.cons` vs)
            | otherwise                    -> v `Q.cons` span_before c p vs

        Right (v@Clump{}, vs)
            | c_chr v /= c || c_pos v >= p -> pure (v `Q.cons` vs)
            | c_pos v + c_len v <= p       -> v `Q.cons` span_before c p vs
            | otherwise                    -> do
                let d = p - c_pos v
                Q.yield $ v { c_len = d }
                pure    $ v { c_pos = p, c_len = c_len v - d, c_refs = dropRS d (c_refs v) } `Q.cons` vs


newtype MinObserved = MinObserved Int
newtype MaxPhysical = MaxPhysical Int

-- | Creates minimal blocks of a constant number of \"observed sites\"
-- and maximum \"physical size\".  The current block is grown until one
-- of the following happens:  (1) If the desired number of \"observed
-- sites\" is reached, the block is yielded.  (2) If a 'Hole' or a new
-- target sequence is hit, the current block is discarded.  (3) If the
-- maximum \"physical size\" is reached, an element from the beginning
-- of the block is discarded so it can grow again.
--
-- An \"observed site\" is a site where every individual has an allele
-- count greater than zero.  In particular, a site without a variant is
-- only \"observed\" if the reference allele is observed at least once
-- in every sample.  A site with 'Variants' is observed if every
-- individual has an alelle count greater than zero for at least one of
-- the listed variants.
--
-- The \"physical size\" of a block is its length in bases, irrespective
-- of the number of 'Variant's, Ns, etc.  Its representation in memory
-- may be considerably more compact than that.
--
-- This logic implies that (1) each block has to be buffered in memory
-- and (2) pieces of the input stream, even pieces containing
-- 'Variant's, can be dropped to make sure all blocks fit within their
-- constraints.  Because of (1), it is best to keep the block size
-- reasonably small.  Because of (2), short blocks are simply dropped.
-- There is no point in trying to preserve some, but not all invalid
-- blocks.  (This is in contrast to 'physicalBlocks', which never drops
-- anything, and may therefore produce short blocks.  It also streams in
-- constant memory.)

observedBlocks :: Monad m => MinObserved -> MaxPhysical -> Stream (Of Clump) m r -> Stream (Stream (Of Clump) m) m r
observedBlocks (MinObserved min_observed) (MaxPhysical max_physical) = go
  where
    go :: Monad m => Stream (Of Clump) m r -> Stream (Stream (Of Clump) m) m r
    go = lift . Q.next >=> \case
        Left         r     -> pure r
        Right (Hole{}, vs) -> go vs
        Right (v,      vs) -> span_block (c_chr v) min_observed max_physical emptyQ (Q.cons v vs)

    -- current target sequence, missing number of observed sites,
    -- leftover physical size, current block
    span_block :: Monad m => Int -> Int -> Int -> ([Clump],[Clump])
                          -> Stream (Of Clump) m r -> Stream (Stream (Of Clump) m) m r
    -- block's done
    span_block !_c 0 !_l q             = (yieldQ q >>) . go

    -- not enough headroom to finish; must drop something
    span_block !c !m  !l q | m > l =
        case popQ q of
            (v,q') -> span_block c (m + num_observed v) (l + phys_size v) q'

    span_block !c !m !l q =
        lift . Q.next >=> \case
            Left r                    -> pure r

            Right (Hole{..}, vs)      -> go vs

            Right (v@Variants{}, vs)
                | c_chr v /= c        -> go (v `Q.cons` vs)
                | otherwise           -> span_block c (m - num_observed v) (l - phys_size v) (pushQ v q) vs

            Right (v@Clump{}, vs)
                | c_chr v /= c        -> go (v `Q.cons` vs)
                | num_observed v <= m -> span_block c (m - num_observed v) (l - phys_size v) (pushQ v q) vs
                | otherwise           -> do
                    -- enough: split correctly, yield, continue
                    yieldQ $ pushQ v { c_len = m } q
                    go     $ v { c_pos = c_pos v + m, c_len = c_len v - m, c_refs = dropRS m (c_refs v) } `Q.cons` vs


    num_observed :: Clump -> Int
    num_observed Hole{}       = 0
    num_observed Clump{..}    = if U.all (> 0) c_refcounts then c_len else 0
    num_observed Variants{..}
        | null c_vars                                               = 0
        | U.and . foldl1' (U.zipWith (||)) $ map is_observed c_vars = 1
        | otherwise                                                 = 0
      where
        is_observed (_ :!: cs) = U.map (\(AC x y) -> x > 0 || y > 0) cs

    phys_size :: Clump -> Int
    phys_size Variants{..} = 1
    phys_size Hole{..}     = c_len
    phys_size Clump{..}    = c_len

    emptyQ          = ([],[])
    pushQ  x (u,v)  = (x:u,v)
    popQ  ([ ],[ ]) = error "popQ: empty"
    popQ  (u,[   ]) = popQ ([], reverse u)
    popQ  (u,(x:v)) = (x,(u,v))
    yieldQ   (u,v)  = yields (Q.each $ v ++ reverse u)


-- | Our variant data comes in as a stream of variants, but for output
-- we need a stream of individuals.  Put another way, the rows of our
-- data matrix are variants, and we need to transpose it so the variants
-- are in the columns.  This means we need to buffer at least a block in
-- memory.
--
-- To keep storage compact, we encode two alleles into a single 'Word8',
-- then store the whole matrix in a 'U.Vector' 'TwoNucs'.  Use 'variants'
-- to access the 'Block' as a list of variants (traverse row-by-row) or
-- 'individuals' to access it as a list of individuals (traverse
-- column-by-column).

data Block = Block !Int                 -- ^ the stride (number of individuals)
                   !(U.Vector TwoNucs)  -- ^ the data matrix

nullBlock :: Block -> Bool
nullBlock (Block _ v) = U.null v

_variants :: Block -> W.Vector (U.Vector TwoNucs)
_variants (Block w ff) =
    W.map (\i -> U.slice i w ff) $
    W.enumFromStepN 0 w h
  where
    h = U.length ff `div` w

individuals :: Block -> W.Vector (U.Vector TwoNucs)
individuals (Block w ff) =
    W.map (\j -> U.map (ff U.!) $ U.enumFromStepN j w h) $
    W.enumFromN 0 w
  where
    h = U.length ff `div` w

