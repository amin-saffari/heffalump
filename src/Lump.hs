module Lump
    ( Lump(..)
    , Clump(..)
    , expandVars
    , expandVarsDense
    , mergeLumps
    , addRef
    , MaxHole(..)
    , lengthLumps
    , PackedLump(..)
    , noLump
    , diff
    , diff2
    , getRefPath
    , encodeLumpToMem
    , decodeMany
    , decodeManyRef
    , fpToSampleName
    , encode
    , encodeHeader
    , decode
    , decodeLump
    , debugLump
    , make_hap
    , normalizeLump
    , encOneVar
    , encTwoVars
    , encTwoNucs
    , Frag(..)
    , patch
    , S(..)
    ) where

import Bio.Prelude                   hiding ( Ns )
import Data.ByteString.Builder              ( Builder, byteString, word8, word32LE )
import Data.ByteString.Unsafe               ( unsafePackMallocCStringLen )
import Foreign.Marshal.Alloc                ( mallocBytes )
import Streaming                            ( Stream, Of(..), MonadIO(..), MonadTrans(..), wrap, yields, mapped, iterTM )

import qualified ByteStream                 as S
import qualified Data.ByteString            as B
import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.Vector                as V
import qualified Data.Vector.Storable       as W
import qualified Data.Vector.Unboxed        as U
import qualified Streaming.Prelude          as Q

import Genome
import Util    ( withInputFile, DataError(..), PebkacError(..), InternalError(..) )

-- | Improved diff representation and encoding.  Goals:
--
-- * support one or two known alleles
-- * do not force SNPs into pairs
-- * allow short indels
-- * allow many operations /without access to the reference/
--
-- Outline:
--
-- We store SNP bases as patch to the reference.  Instead of a base, we
-- store the change needed:  none, transition, complement,
-- trans-complement.  Deletions just have a length, insertions have a
-- length and a sequence of bases.  Complex alleles (such as two different
-- alleles) would need to be broken up into separate variants---haven't
-- thought about the details.
--
-- The code we use:
--
--      0b00000000: break
--
--      0b00000001: transition
--      0b00000010: complement
--      0b00000011: trans-complement
--
--      0b00000101: ref+trans
--      0b00000110: trans+trans
--      0b00000111: ref+compl
--      0b00001000: trans+compl
--      0b00001001: compl+compl
--      0b00001010: ref+tcompl
--      0b00001011: trans+tcompl
--      0b00001100: compl+tcompl
--      0b00001101: tcompl+tcompl
--
--      0b0001yxxx: monoallelic indel
--      0b0010yxxx: heterozygous indel
--      0b0011yxxx: diallelic indel
--          if xxx == 0, one length byte follows.
--          if y == 1, it's an insertion and xxx bases follow (2 bits per base,
--                     ATGC, padded to full bytes)
--
--      0b01xxxxxx: short uncalled stretch
--      0b10xxxxxx: short monoallelic matches
--      0b11xxxxxx: short diallelic matches
--          if xxxxxx == 111111, one length byte follows
--          if xxxxxx == 111110, two length bytes follow
--          if xxxxxx == 111101, three length bytes follow
--          if xxxxxx == 111100, four length bytes follow
--
-- We are left with three "reserved" codes (0x04, 0x0E, 0x0F) and three
-- nonsensical ones (0x40, 0x80, 0xC0).
--
-- Note the bases: bit 0 codes for transition, bit 1 for complement.
-- We might sometimes want to output variants without having a reference
-- available.  In that case, we code as follows:  I (Identical), O
-- (transitiOn), P (comPlement), X (trans-complement)
--
-- If a 'Break' is encountered before the reference ends, the remainder
-- of that reference is implied to be 'Ns'.

data Lump
    = Ns !Int                          -- ^ uncalled stretch

    | Eqs1 !Int                        -- ^ stretch of monoallelic matches
    | Trans1                           -- ^ monoallelic transversion
    | Compl1                           -- ^ monoallelic complement
    | TCompl1                          -- ^ monoallelic trans-complement

    | Eqs2 !Int                        -- ^ stretch of diallelic matches
    | RefTrans                         -- ^ nine more diallelic SNP codes (*groan*)
    | Trans2
    | RefCompl
    | TransCompl
    | Compl2
    | RefTCompl
    | TransTCompl
    | ComplTCompl
    | TCompl2

    | Del1 !Int                        -- ^ deletion with other allele unknown
    | Del2 !Int                        -- ^ homozygous deletion
    | DelH !Int                        -- ^ deletion, the other allele is the reference

    | Ins1 !Seq1                       -- ^ insertion with other allele unknown
    | Ins2 !Seq1                       -- ^ homozygous insertion
    | InsH !Seq1                       -- ^ insertion, the other allele is the reference

    | Break                            -- ^ break marker (end-of-chromosome)
  deriving (Show, Eq)

type Seq1 = U.Vector Word8

debugLump :: Stream (Of Lump) IO r -> IO r
debugLump = go (0::Int) (0::Int)
  where
    go c i = Q.next >=> \case
      Left r -> return r
      Right (x,l) -> do
            putStrLn $ shows (c,i) "\t" ++ show x
            case x of
                Break  -> go (c+1) 0 l
                Ns   n -> go c (i+n) l
                Eqs1 n -> go c (i+n) l
                Eqs2 n -> go c (i+n) l

                Del1 _ -> go c i l
                Del2 _ -> go c i l
                DelH _ -> go c i l

                Ins1 _ -> go c i l
                Ins2 _ -> go c i l
                InsH _ -> go c i l

                _      -> go c (i+1) l

lengthLumps :: Monad m => Stream (Of Lump) m r -> Stream (Of Int) m r
lengthLumps = mapped (Q.fold (\a -> (+) a . ln) 0 id) . Q.breaks isBreak
  where
    isBreak :: Lump -> Bool
    isBreak Break = True
    isBreak _     = False

    ln :: Lump -> Int
    ln (Ns n) = n
    ln (Eqs1 n) = n
    ln (Eqs2 n) = n

    ln (Del1 n) = n
    ln (Del2 n) = n
    ln (DelH n) = n

    ln (Ins1 s) = U.length s
    ln (Ins2 s) = U.length s
    ln (InsH s) = U.length s

    ln  Break = 0
    ln  _     = 1

normalizeLump :: Monad m => Stream (Of Lump) m r -> Stream (Of Lump) m r
normalizeLump = Q.unfoldr (Q.next >=> either (return . Left) go)
  where
    wrapE = either pure (uncurry Q.cons)

    go (Ns 0,l) = Q.next l
    go (Ns n,l) = Q.next l >>= \case
        Right (Ns n',l') -> go (Ns (n+n'),l')
        l'               -> return $ Right (Ns n, wrapE l')

    go (Eqs1 0,l) = Q.next l
    go (Eqs1 n,l) = Q.next l >>= \case
        Right (Eqs1 n',l') -> go (Eqs1 (n+n'),l')
        l'                 -> return $ Right (Eqs1 n, wrapE l')

    go (Eqs2 0,l) = Q.next l
    go (Eqs2 n,l) = Q.next l >>= \case
        Right (Eqs2 n',l') -> go (Eqs2 (n+n'),l')
        l'                 -> return $ Right (Eqs2 n, wrapE l')

    go (Del1 0,l) = Q.next l
    go (Del1 n,l) = Q.next l >>= \case
        Right (Del1 n',l') -> go (Del1 (n+n'),l')
        l'                 -> return $ Right (Del1 n, wrapE l')

    go (Del2 0,l) = Q.next l
    go (Del2 n,l) = Q.next l >>= \case
        Right (Del2 n',l') -> go (Del2 (n+n'),l')
        l'                 -> return $ Right (Del2 n, wrapE l')

    go (DelH 0,l) = Q.next l
    go (DelH n,l) = Q.next l >>= \case
        Right (DelH n',l') -> go (DelH (n+n'),l')
        l'                 -> return $ Right (DelH n, wrapE l')

    go (Ins1 s,l) | U.null s = Q.next l
    go (Ins1 s,l) = Q.next l >>= \case
        Right (Ins1 s',l') -> go (Ins1 (s U.++ s'),l')
        l'                 -> return $ Right (Ins1 s, wrapE l')

    go (Ins2 s,l) | U.null s = Q.next l
    go (Ins2 s,l) = Q.next l >>= \case
        Right (Ins2 s',l') -> go (Ins2 (s U.++ s'),l')
        l'                 -> return $ Right (Ins2 s, wrapE l')

    go (InsH s,l) | U.null s = Q.next l
    go (InsH s,l) = Q.next l >>= \case
        Right (InsH s',l') -> go (InsH (s U.++ s'),l')
        l'                 -> return $ Right (InsH s, wrapE l')

    go lump = return $ Right lump

-- | Lowers all calls to haploid.
-- Applying this to most inputs can only do harm, but it is valuable on
-- EMF and MAF.
make_hap :: Lump -> Lump
make_hap  Trans2  = Trans1
make_hap  Compl2  = Compl1
make_hap TCompl2  = TCompl1
make_hap (Eqs2 n) = Eqs1 n
make_hap (Del2 n) = Del1 n
make_hap (Ins2 s) = Ins1 s
make_hap       x  = x

newtype PackedLump = PackLump { unpackLump :: L.ByteString }

-- | The encoding of an empty chromosome:  just the 'Break'
{-# INLINE noLump #-}
noLump :: PackedLump
noLump = PackLump (L.singleton '\0')

{-# INLINE encodeLumpToMem #-}
encodeLumpToMem :: MonadIO m => Stream (Of Lump) m r -> m (Of PackedLump r)
encodeLumpToMem = liftM (Q.mapOf PackLump) . S.toLazy . encodeLump . normalizeLump

-- | Encodes a 'Lump' to a compact binary representation.  Some
-- functionality is duplicated in 'encodeGenome' and 'noLump'; if the
-- encoding is ever changed, it needs to be tracked over there.
{-# INLINE encodeLump #-}
encodeLump :: MonadIO m => Stream (Of Lump) m r -> S.ByteStream m r
encodeLump = go
  where
    go s = S.mwrap $ do p <- liftIO $ mallocBytes 0x8000
                        (o,r) <- fill p 0x8000 s 0
                        c <- liftIO $ unsafePackMallocCStringLen (castPtr p,0x8000)
                        return $ do S.chunk (B.take o c)
                                    either pure go r

    -- An insert is never longer than 255, so it needs no more than
    -- ceil(255/4)+2 bytes, hence the magic 66 below.  Once we have 66
    -- bytes, the next code is guaranteed to fit.
    fill :: MonadIO m => Ptr Word8 -> Int -> Stream (Of Lump) m r -> Int -> m (Int, Either r (Stream (Of Lump) m r))
    fill p l s o
        | l - o < 66 = return (o, Right s)      -- need more space
        | otherwise  = Q.next s >>= either (return . (,) o . Left) (\case
            (Ns n,s')                         -> stretchOf 0x40 n >>= fill p l s'
            (Eqs1 n,s')                       -> stretchOf 0x80 n >>= fill p l s'
            (Trans1,s')                       -> wrd8 0x01 >>= fill p l s'
            (Compl1,s')                       -> wrd8 0x02 >>= fill p l s'
            (TCompl1,s')                      -> wrd8 0x03 >>= fill p l s'

            (Eqs2 n,s')                       -> stretchOf 0xC0 n >>= fill p l s'
            (RefTrans,s')                     -> wrd8 0x05 >>= fill p l s'
            (Trans2,s')                       -> wrd8 0x06 >>= fill p l s'
            (RefCompl,s')                     -> wrd8 0x07 >>= fill p l s'
            (TransCompl,s')                   -> wrd8 0x08 >>= fill p l s'
            (Compl2,s')                       -> wrd8 0x09 >>= fill p l s'
            (RefTCompl,s')                    -> wrd8 0x0A >>= fill p l s'
            (TransTCompl,s')                  -> wrd8 0x0B >>= fill p l s'
            (ComplTCompl,s')                  -> wrd8 0x0C >>= fill p l s'
            (TCompl2,s')                      -> wrd8 0x0D >>= fill p l s'

            (Del1 n,s')                       -> indelOf 0x10 n >>= fill p l s'
            (DelH n,s')                       -> indelOf 0x20 n >>= fill p l s'
            (Del2 n,s')                       -> indelOf 0x30 n >>= fill p l s'

            (Ins1 sq,s')                      -> indelOf 0x18 (U.length sq) >>= seqOf sq >>= fill p l s'
            (InsH sq,s')                      -> indelOf 0x28 (U.length sq) >>= seqOf sq >>= fill p l s'
            (Ins2 sq,s')                      -> indelOf 0x38 (U.length sq) >>= seqOf sq >>= fill p l s'

            (Break,s')                        -> wrd8 0x00 >>= fill p l s')
      where
        stretchOf k n
            | n < 0           = liftIO . throwIO $ DataError Nothing ("negative stretch length " ++ show n)
            | n == 0          =             return o
            | n < 0x3C        = liftIO $ do pokeElemOff p o (k .|. fromIntegral n)
                                            return $ o+1
            | n < 0x100       = liftIO $ do pokeElemOff p (o+0) (k .|. 0x3F)
                                            pokeElemOff p (o+1) (fromIntegral  n)
                                            return $ o+2
            | n < 0x10000     = liftIO $ do pokeElemOff p (o+0) (k .|. 0x3E)
                                            pokeElemOff p (o+1) (fromIntegral (n             .&. 0xff))
                                            pokeElemOff p (o+2) (fromIntegral (n `shiftR`  8 .&. 0xff))
                                            return $ o+3
            | n < 0x1000000   = liftIO $ do pokeElemOff p (o+0) (k .|. 0x3D)
                                            pokeElemOff p (o+1) (fromIntegral (n             .&. 0xff))
                                            pokeElemOff p (o+2) (fromIntegral (n `shiftR`  8 .&. 0xff))
                                            pokeElemOff p (o+3) (fromIntegral (n `shiftR` 16 .&. 0xff))
                                            return $ o+4
            | n < 0x100000000 = liftIO $ do pokeElemOff p (o+0) (k .|. 0x3C)
                                            pokeElemOff p (o+1) (fromIntegral (n             .&. 0xff))
                                            pokeElemOff p (o+2) (fromIntegral (n `shiftR`  8 .&. 0xff))
                                            pokeElemOff p (o+3) (fromIntegral (n `shiftR` 16 .&. 0xff))
                                            pokeElemOff p (o+4) (fromIntegral (n `shiftR` 24 .&. 0xff))
                                            return $ o+5
            | otherwise       = liftIO . throwIO $ DataError Nothing ("stretch is too long: " ++ show n)

        indelOf k n
            | n < 0         = liftIO . throwIO $ DataError Nothing "indel of negative length"
            | n == 0        = return o
            | n < 8         = liftIO $ pokeElemOff p o (k .|. fromIntegral n) >> return (o+1)
            | n < 0x100     = liftIO $ pokeElemOff p o k >> pokeElemOff p (o+1) (fromIntegral n) >> return (o+2)
            | otherwise     = liftIO . throwIO . DataError Nothing $ "long indel of" ++ show n

        seqOf sq oo
            | U.length sq == 0 = return oo
            | U.length sq == 1 = liftIO $ pokeElemOff p oo (U.unsafeIndex sq 0) >> return (oo+1)
            | U.length sq == 2 = liftIO $ pokeElemOff p oo (U.unsafeIndex sq 0 .|.
                                                            U.unsafeIndex sq 1 `shiftL` 2) >> return (oo+1)
            | U.length sq == 3 = liftIO $ pokeElemOff p oo (U.unsafeIndex sq 0 .|.
                                                            U.unsafeIndex sq 1 `shiftL` 2 .|.
                                                            U.unsafeIndex sq 2 `shiftL` 4) >> return (oo+1)
            | otherwise        = liftIO (pokeElemOff p oo (U.unsafeIndex sq 0 .|.
                                                           U.unsafeIndex sq 1 `shiftL` 2 .|.
                                                           U.unsafeIndex sq 2 `shiftL` 4 .|.
                                                           U.unsafeIndex sq 3 `shiftL` 6))
                                 >> seqOf (U.unsafeDrop 4 sq) (oo+1)

        wrd8 :: MonadIO m => Word8 -> m Int
        wrd8 w = liftIO $ pokeElemOff p o w >> return (o+1)

decodeLump :: Monad m => Maybe FilePath -> S.ByteStream m r -> Stream (Of Lump) m r
decodeLump fp = Q.unfoldr decode1
  where
    decode1 = S.nextByte >=> \case
        Left r                                 -> return $ Left r
        Right (c,s1)
            | c == 0x00                        -> return $ Right (Break,s1)
            | c == 0x01                        -> return $ Right (Trans1,s1)
            | c == 0x02                        -> return $ Right (Compl1,s1)
            | c == 0x03                        -> return $ Right (TCompl1,s1)
            | c == 0x05                        -> return $ Right (RefTrans,s1)
            | c == 0x06                        -> return $ Right (Trans2,s1)
            | c == 0x07                        -> return $ Right (RefCompl,s1)
            | c == 0x08                        -> return $ Right (TransCompl,s1)
            | c == 0x09                        -> return $ Right (Compl2,s1)
            | c == 0x0A                        -> return $ Right (RefTCompl,s1)
            | c == 0x0B                        -> return $ Right (TransTCompl,s1)
            | c == 0x0C                        -> return $ Right (ComplTCompl,s1)
            | c == 0x0D                        -> return $ Right (TCompl2,s1)

            | c .&. 0xF8 == 0x10               -> del_of Del1 c s1
            | c .&. 0xF8 == 0x20               -> del_of DelH c s1
            | c .&. 0xF8 == 0x30               -> del_of Del2 c s1

            | c .&. 0xF8 == 0x18               -> ins_of Ins1 c s1
            | c .&. 0xF8 == 0x28               -> ins_of InsH c s1
            | c .&. 0xF8 == 0x38               -> ins_of Ins2 c s1

            | c .&. 0xC0 == 0x40               -> stretch_of Ns c s1
            | c .&. 0xC0 == 0x80               -> stretch_of Eqs1 c s1
            | c .&. 0xC0 == 0xC0               -> stretch_of Eqs2 c s1

            | otherwise {- 0x04, 0x0E, 0x0F -} -> throw . DataError fp $ "unexpected code " ++ show c

    stretch_of cons c s1
        | c .&. 0x3F == 0x3F  = do Right (w0,s2) <- S.nextByte s1
                                   return $ Right (cons (fromIntegral w0),s2)
        | c .&. 0x3F == 0x3E  = do Right (w0,s2) <- S.nextByte s1
                                   Right (w1,s3) <- S.nextByte s2
                                   return $ Right (cons (fromIntegral w0 .|.
                                                         fromIntegral w1 `shiftL`  8),s3)
        | c .&. 0x3F == 0x3D  = do Right (w0,s2) <- S.nextByte s1
                                   Right (w1,s3) <- S.nextByte s2
                                   Right (w2,s4) <- S.nextByte s3
                                   return $ Right (cons (fromIntegral w0 .|.
                                                         fromIntegral w1 `shiftL`  8 .|.
                                                         fromIntegral w2 `shiftL` 16),s4)
        | c .&. 0x3F == 0x3C  = do Right (w0,s2) <- S.nextByte s1
                                   Right (w1,s3) <- S.nextByte s2
                                   Right (w2,s4) <- S.nextByte s3
                                   Right (w3,s5) <- S.nextByte s4
                                   return $ Right (cons (fromIntegral w0 .|.
                                                         fromIntegral w1 `shiftL`  8 .|.
                                                         fromIntegral w2 `shiftL` 16 .|.
                                                         fromIntegral w3 `shiftL` 24),s5)
        | otherwise           = return $ Right (cons (fromIntegral (c .&. 0x3F)),s1)

    del_of cons c s1
        | c .&. 0x07 == 0  = do Right (hd,tl) <- S.nextByte s1
                                return $ Right (cons (fromIntegral hd),tl)
        | otherwise        = return $ Right (cons (fromIntegral c .&. 0x07),s1)

    ins_of cons c s1
        | c .&. 0x07 == 0  = do Right (hd,tl) <- S.nextByte s1
                                seq_of cons U.empty hd tl
        | otherwise        = seq_of cons U.empty (c .&. 0x07) s1



    seq_of cons acc 0 s1 = return $ Right (cons acc,s1)

    seq_of cons acc 1 s1 = do
        Right (hd,tl) <- S.nextByte s1
        let acc' = acc `U.snoc` (hd `shiftR` 0 .&. 0x3)
        return $ Right (cons acc',tl)

    seq_of cons acc 2 s1 = do
        Right (hd,tl) <- S.nextByte s1
        let acc' = acc `U.snoc` (hd `shiftR` 0 .&. 0x3)
                       `U.snoc` (hd `shiftR` 2 .&. 0x3)
        return $ Right (cons acc',tl)

    seq_of cons acc 3 s1 = do
        Right (hd,tl) <- S.nextByte s1
        let acc' = acc `U.snoc` (hd `shiftR` 0 .&. 0x3)
                       `U.snoc` (hd `shiftR` 2 .&. 0x3)
                       `U.snoc` (hd `shiftR` 4 .&. 0x3)
        return $ Right (cons acc',tl)

    seq_of cons acc n s1 = do
        Right (hd,tl) <- S.nextByte s1
        let acc' = acc `U.snoc` (hd `shiftR` 0 .&. 0x3)
                       `U.snoc` (hd `shiftR` 2 .&. 0x3)
                       `U.snoc` (hd `shiftR` 4 .&. 0x3)
                       `U.snoc` (hd `shiftR` 6 .&. 0x3)
        seq_of cons acc' (n-4) tl


-- | 'Lump's from multiple samples will form 'Clump's
data Clump
    = Clump                                                     -- ^ a clump of invariant sites
        { c_chr       :: !Int                                   -- ^ chromosome number
        , c_pos       :: !Int                                   -- ^ 0-based position
        , c_len       :: !Int                                   -- ^ length of homogenous stretch
        , c_refs      :: RefSeq                                 -- ^ reference sequence, sometimes 'RefEnd'
        , c_refcounts :: !(W.Vector Word8) }                    -- ^ reference allele counts

    | Hole                                                      -- ^ a hole in the reference
        { c_chr       :: !Int                                   -- ^ chromosome number
        , c_pos       :: !Int                                   -- ^ 0-based position
        , c_len       :: !Int }                                 -- ^ length of hole

    | Variants                                                  -- ^ a site with variants
        { c_chr   :: !Int                                       -- ^ chromosome number
        , c_pos   :: !Int                                       -- ^ 0-based position
        , c_ref   :: !Nuc2b                                     -- ^ reference allele, sometimes N
        , c_vars  :: ![Pair Var2b (W.Vector AlleleCounts)] }    -- ^ alt alleles and allele counts for each variant

-- | Merging without access to a reference sequence.  This code doesn't
-- believe in Indels and skips over them.
--
-- 'noutgroups':  number of outgroups

expandVars :: Monad m => Int -> Stream (Of Clump) m r -> Stream (Of [Variant]) m r
expandVars !noutgroups = Q.filter (not . null) . Q.map to_vars
  where
    has_alt (AC _ c) = c /= 0

    to_vars Clump   {  } = [ ]
    to_vars Hole    {  } = [ ]
    to_vars Variants{..} = [ Variant c_chr c_pos c_ref alt cs
                           | alt :!: cs <- c_vars
                           -- it's only a variant if at least one alt called
                           , W.any has_alt $ W.drop noutgroups cs ]

-- like expandVars, but produces at least one Variant for each site
-- outside a 'Hole'.
expandVarsDense :: Monad m => Stream (Of Clump) m r -> Stream (Of [Variant]) m r
expandVarsDense = Q.filter (not . null) . iterTM to_vars
  where
    to_vars (Clump{..} :> k) = go c_pos c_refs >> k
      where
        !cs = W.map (flip AC 0) c_refcounts

        go !pos !ref
            | pos == c_pos + c_len = pure ()
            | otherwise =
                let (r,ref') = fromMaybe (N2b 255, RefEnd) (unconsRS ref)
                in [ Variant c_chr pos r (V2b 255) cs ] `Q.cons` go (succ pos) ref'

    to_vars (Hole{..} :> k) = k

    to_vars (Variants{..} :> k) =
        -- any vars with alt calls?
        case    [ Variant c_chr c_pos c_ref alt cs
                | alt :!: cs <- c_vars
                , W.any ((/= 0) . ac_num_alt) cs ] of

           [] -> case c_vars of
                    -- none: create a fake variant with N for alt
                    (_ :!: cs) : _ -> [ Variant c_chr c_pos c_ref (V2b 255) cs ] `Q.cons` k
                    -- none, but nothing to work with
                    []             -> k

           vs -> vs `Q.cons` k


newtype MaxHole = MaxHole Int

-- | Adds information from the reference.  This fills the 'c_ref' field
-- and converts whatever overlaps Ns in the reference into 'Hole's.
-- More specifically, up to @mh@ Ns are passed on as Ns; any more become
-- a single 'Hole'.
--
-- We walk along the stream, which is efficient if few 'Variants' come in,
-- blindly fill the reference allele in the case of 'Variants', pass
-- 'Hole's along, and split 'Clump's into 'Hole's and 'Clump's with an
-- associated reference sequence.  While the code could produce
-- 'Variants' with an N for the reference allele, or adjacent 'Hole's,
-- this will never happen for canonical input.  If it does happen, it's
-- probably no big deal.
addRef :: Monad m => MaxHole -> RefSeqs -> Stream (Of Clump) m r -> Stream (Of Clump) m r
addRef (MaxHole mh) ref = go 0 (rss_seqs ref)
  where
    go :: Monad m => Int -> [() -> RefSeq] -> Stream (Of Clump) m r -> Stream (Of Clump) m r
    go !_ [     ] = lift . Q.effects
    go !c (r0:rs) = go1 (r0 ()) 0
      where
        go1 !r !p = lift . Q.next >=> \case
            Left x              -> pure x
            Right (cl, cls)
                | c /= c_chr cl -> go (c+1) rs (Q.cons cl cls)
                | p == c_pos cl -> go2 cl cls
                | p < c_pos cl  -> go1 (dropRS (c_pos cl - p) r) (c_pos cl) (Q.cons cl cls)
                | otherwise     -> throw $ DataError Nothing "[addRef] expected sorted variants"
          where
            go2 v@Variants{..} cls =
                case unconsRS r of
                    Just (a,r') -> v { c_ref = a } `Q.cons` go1   r'   (p+1) cls
                    Nothing     -> v               `Q.cons` go1 RefEnd (p+1) cls

            go2 h@Hole{..} cls = h `Q.cons` go1 (dropRS c_len r) (p + c_len) cls

            go2 Clump{..} cls = case r of
                ManyNs n r' | n > mh    -> go3N n r'
                            | otherwise -> go3S n r'
                SomeSeq _ _ n r'        -> go3S n r'
                RefEnd                  -> Hole{..} `Q.cons` go1 RefEnd (c_pos+c_len) cls

              where
                go3N n r'
                    | n >= c_len = Hole{..} `Q.cons` go1 (dropRS c_len r) (c_pos + c_len) cls
                    | otherwise  = Hole{ c_len = n, .. } `Q.cons` go1 r' (c_pos + n) (Q.cons cl' cls)
                      where
                        cl' = Clump { c_pos = c_pos + n, c_len = c_len - n, .. }
                go3S len r'
                    | len >= c_len = Clump{ c_refs = r, .. } `Q.cons` go1 (dropRS c_len r) (c_pos + c_len) cls
                    | otherwise    = Clump{ c_refs = r, c_len = len, .. } `Q.cons` go1 r' (c_pos + len) (Q.cons cl' cls)
                      where
                        cl' = Clump { c_pos = c_pos + len, c_len = c_len - len, .. }



-- | Merges multiple streams of 'Lump's into 'Clump's.  For efficiency
-- reasons, we try to produce long stretches whereever possible, and
-- anything that breaks the stretches turns into 'Variants' records.
--
-- We do not want outgroups to break the stretches, so they are treated
-- specially:  a variant present only in outgroups is ignored and is
-- subsumed into the preceeding stretch.  That makes the reference
-- allele counts for outgroups nonsensical, so we cheat by setting it to
-- one.  Put another way, we pretend the outgroups are haploid and match
-- the reference, unless we have a reason to generate a 'Variant'
-- anyway, in which case we check for the actual state.
--
-- We make no use of the reference genome here, so the 'c_ref' field
-- will always be @(N2b 255)@, and no 'Hole's are generated.

mergeLumps :: Monad m => Int -> V.Vector (Stream (Of Lump) m ()) -> Stream (Of Clump) m ()
mergeLumps noutgroups = go 0 0
  where
    go :: Monad m => Int -> Int -> V.Vector (Stream (Of Lump) m ()) -> Stream (Of Clump) m ()
    go !ix !pos = lift . V.mapM Q.uncons >=> \case
             -- all samples 'Done', no more 'Variant's to produce
        smps | V.all isNothing smps -> pure ()

             -- all samples 'Break' or are done, next chromosome
             | V.all isBreak   smps -> go (succ ix) 0 (V.map dropBreak smps)

             -- We ignore outgroups in computing the longest stretch.
             | otherwise ->
                -- pure operation, doesn't consume the streams
                let msmps = V.map (fmap fst) smps
                in case V.minimum . V.map clumplen $ V.drop noutgroups msmps of
                    -- no stretch, have to look for vars
                    0 -> mkVars ix pos msmps `Q.cons`
                         go ix (succ pos) (unS $ V.mapM (S . dropLumpM 1) smps)

                    -- a clump of l non-variants
                    l -> mkClump ix pos l msmps `Q.cons`
                         go ix (pos + fromIntegral l) (unS $ V.mapM (S . dropLumpM l) smps)

    -- Find all the variants, anchored on the reference allele, and
    -- split them.  Misfitting alleles are not counted.
    mkVars :: Int -> Int -> V.Vector (Maybe Lump) -> Clump
    mkVars ix pos ss = Variants ix pos (N2b 255)
                                [ V2b alt :!: calls
                                | (alt, ct) <- zip [1..3] [ct_trans, ct_compl, ct_tcompl]
                                , let calls = V.convert $ V.map (maybe (AC 0 0) ct) ss ]

    mkClump ix pos ln ss = Clump ix pos ln RefEnd (V.convert $ V.map (maybe 0 ct_refs) ss)

    clumplen (Just (Ns   n)) = n
    clumplen (Just (Eqs1 n)) = n
    clumplen (Just (Eqs2 n)) = n
    clumplen (Just  Break  ) = maxBound
    clumplen  Nothing        = maxBound
    clumplen  _              = 0

    isBreak :: Maybe (Lump, b) -> Bool
    isBreak  Nothing         = True
    isBreak (Just (Break,_)) = True
    isBreak              _   = False


dropBreak :: Monad m => Maybe (Lump, Stream (Of Lump) m ()) -> Stream (Of Lump) m ()
dropBreak  Nothing         =    pure ()
dropBreak (Just (Break,s)) =          s
dropBreak (Just     (a,s)) = Q.cons a s

-- Skip over exactly l sites.
dropLumpS :: Monad m => Int -> Stream (Of Lump) m () -> Stream (Of Lump) m ()
dropLumpS !l = lift . Q.uncons >=> dropLumpM l

dropLumpM :: Monad m => Int -> Maybe (Lump, Stream (Of Lump) m ()) -> Stream (Of Lump) m ()
dropLumpM !_  Nothing  = pure ()
dropLumpM !l (Just  s) = dropLump l s

dropLump :: Monad m => Int -> (Lump, Stream (Of Lump) m ()) -> Stream (Of Lump) m ()
dropLump !l _ | l < 0  = throw $ InternalError "dropLump: negative amount"

dropLump !l (Del1 _,s) = dropLumpS l s
dropLump !l (Del2 _,s) = dropLumpS l s
dropLump !l (DelH _,s) = dropLumpS l s

dropLump !l (Ins1 _,s) = dropLumpS l s
dropLump !l (Ins2 _,s) = dropLumpS l s
dropLump !l (InsH _,s) = dropLumpS l s

dropLump !_ (Break, s) = Break `Q.cons` s

dropLump !l (Ns   !n,s) | l == n    = s
                        | l <  n    = Ns (n-l) `Q.cons` s
                        | otherwise = dropLumpS (l-n) s
dropLump !l (Eqs1 !n,s) | l == n    = s
                        | l <  n    = Eqs1 (n-l) `Q.cons` s
                        | otherwise = dropLumpS (l-n) s
dropLump !l (Eqs2 !n,s) | l == n    = s
                        | l <  n    = Eqs2 (n-l) `Q.cons` s
                        | otherwise = dropLumpS (l-n) s

dropLump  0      (a,s) = Q.cons a s
dropLump !l      (_,s) = dropLumpS (l-1) s


ct_refs :: Lump -> Word8
ct_refs (Eqs1      _) = 1
ct_refs (Eqs2      _) = 2
ct_refs  _            = 0

-- Variant codes:  #ref + 4 * #alt
ct_trans :: Lump -> AlleleCounts
ct_trans (Eqs1      _) = AC 1 0
ct_trans  Trans1       = AC 0 1

ct_trans (Eqs2      _) = AC 2 0
ct_trans  RefTrans     = AC 1 1
ct_trans  Trans2       = AC 0 2
ct_trans  RefCompl     = AC 1 0
ct_trans  TransCompl   = AC 0 1
ct_trans  RefTCompl    = AC 1 0
ct_trans  TransTCompl  = AC 0 1
ct_trans  _            = AC 0 0

ct_compl :: Lump -> AlleleCounts
ct_compl (Eqs1      _) = AC 1 0
ct_compl  Compl1       = AC 0 1

ct_compl (Eqs2      _) = AC 2 0
ct_compl  RefTrans     = AC 1 0
ct_compl  RefCompl     = AC 1 1
ct_compl  TransCompl   = AC 0 1
ct_compl  Compl2       = AC 0 2
ct_compl  RefTCompl    = AC 1 0
ct_compl  ComplTCompl  = AC 0 1
ct_compl  _            = AC 0 0

ct_tcompl :: Lump -> AlleleCounts
ct_tcompl (Eqs1      _) = AC 1 0
ct_tcompl  TCompl1      = AC 0 1

ct_tcompl (Eqs2      _) = AC 2 0
ct_tcompl  RefTrans     = AC 1 0
ct_tcompl  RefCompl     = AC 1 0
ct_tcompl  RefTCompl    = AC 1 1
ct_tcompl  TransTCompl  = AC 0 1
ct_tcompl  ComplTCompl  = AC 0 1
ct_tcompl  TCompl2      = AC 0 2
ct_tcompl  _            = AC 0 0

-- | This gunk is need to make a map over a 'Vector' strict.  Looks
-- ugly, but is perfectly servicable.
newtype S a = S { unS :: a }

instance Functor S where
    {-# INLINE fmap #-}
    fmap f (S a) = S (f a)

instance Applicative S where
    {-# INLINE pure #-}
    pure = S
    {-# INLINE (<*>) #-}
    S f <*> S a = S (f a)

instance Monad S where
    {-# INLINE return #-}
    return = S
    {-# INLINE (>>=) #-}
    S !a >>= k = k a


-- | Main decoder.  Decodes only the current format.  If a reference
-- genome is supplied, it is checked for compatibility, but not actually
-- used for anything.
decode :: Monad m => Maybe RefSeqs -> Maybe FilePath -> S.ByteStream m r -> Stream (Of Lump) m r
decode mref fp str = do
    hd :> tl <- lift . S.toStrict $ S.splitAt 4 str
    case B.take 3 hd of
        "HEF" -> case B.uncons $ B.drop 3 hd of
            Nothing                -> unknown_file
            Just (c,_) | c <  3    -> old_file
                       | c == 3    -> go tl mref
                       | otherwise -> new_file
        "\176\113\2"               -> old_file
        "\177\113\2"               -> old_file
        _                          -> unknown_file
  where
    old_file     = throw $ DataError fp "Legacy hef file, you may want to run 'heffalump legacy' on it."
    new_file     = throw $ DataError fp "Hef from the future, you need to upgrade heffalump."
    unknown_file = throw $ DataError fp "File format not recognized."

    go s  Nothing  = decodeLump fp . S.drop 5 $ S.dropWhile (/= 0) s
    go s (Just  r) = do hs :> s' <- lift . S.toStrict . S.splitAt 4 . S.drop 1 . S.dropWhile (/= 0) $ s
                        let hv = B.foldr (\b a -> fromIntegral b .|. shiftL a 8) 0 hs :: Word32
                        if hv == hash_ref r
                            then decodeLump fp s'
                            else throw $ PebkacError "Incompatible reference genome."


getRefPath :: Monad m => S.ByteStream m r -> m (Maybe FilePath, S.ByteStream m r)
getRefPath str = do
    key :> rest <- S.toStrict $ S.splitAt 4 str
    if "HEF\3" == key
      then do fp :> rest' <- S.toStrict $ S.break (== 0) rest
              return ( Just $ unpack fp, S.chunk key >> S.chunk fp >> rest' )
      else return ( Nothing, S.chunk key >> rest )

fpToSampleName :: FilePath -> Bytes
fpToSampleName = fromString . takeWhile (/='.') . reverse . takeWhile (/='/') . reverse

-- | Runs a computation with many decoded heffalumps.  (I don't like to
-- infect everything with 'ResourceT', and apparently the only way
-- around it is to use 'withFile', so we might as well do it right in
-- here.)  No reference genome is needed, nor is it used if available,
-- nor is it provided to the continuation.
decodeMany :: [FilePath] -> ( V.Vector (Stream (Of Lump) IO ()) -> IO r ) -> IO r
decodeMany fs kk =
    withFiles fs $
        kk . V.fromList . map (\(fp,hdl) -> decode Nothing (Just fp) (S.decomp $ S.hGetContents hdl))
  where
    withFiles [      ] k = k []
    withFiles (fp:fps) k =
        withInputFile fp $ \hdl ->
            withFiles fps $
                k . (:) (fp,hdl)

-- | Runs a computation with many decoded heffalumps.  (I don't like to
-- infect everything with 'ResourceT', and apparently the only way
-- around it is to use 'withFile', so we might as well do it right in
-- here.)  It looks for a reference genome and fails if none can be
-- found.  A path can be supplied instead of having it search.  The
-- reference is supplied to the continuation.
decodeManyRef :: Maybe FilePath -> [FilePath]
              -> ( RefSeqs -> V.Vector (Stream (Of Lump) IO ()) -> IO r ) -> IO r
decodeManyRef mfp fs kk =
    withFiles fs $ \hdls -> do
        (rps,raws) <- unzip <$> mapM (getRefPath . S.decomp . S.hGetContents . snd) hdls
        mrs <- case mfp of
                Just fp -> Right <$> liftIO (readTwoBit fp)
                Nothing -> liftIO . findGenome . catMaybes $ zipWith (\a mb -> (,) a <$> mb) fs rps
        case mrs of
            Left msg -> throwIO $ PebkacError msg
            Right rs -> kk rs (V.fromList $ zipWith (decode (Just rs) . Just) (map fst hdls) raws)
  where
    withFiles [      ] k = k []
    withFiles (fp:fps) k =
        withInputFile fp $ \hdl ->
            withFiles fps $
                k . (:) (fp,hdl)

-- | Encode a 'Lump' and enough information about the 'RefSeqs' to be
-- (1) able to find it again and (2) to make sure we got the right one
-- when operating on a 'Lump'.
encode :: MonadIO m => RefSeqs -> Stream (Of Lump) m r -> S.ByteStream m r
encode r s = do S.toByteStream (encodeHeader r)
                encodeLump s

encodeHeader :: RefSeqs -> Builder
encodeHeader r = byteString "HEF\3" <>
                 byteString (rss_path r) <> word8 0 <>
                 word32LE (hash_ref r)

-- | Diff between a reference and a sample in string format.  The sample
-- is treated as diploid.
--
-- XXX  We should deal with gaps, now that we can actually encode them
{-# INLINE gendiff #-}
gendiff :: Monad m => (a -> RefSeqView a) -> a -> S.ByteStream m r -> Stream (Of Lump) m r
gendiff view = generic
  where
    isN        c = c == c2w 'N' || c == c2w 'n' || c == c2w '-'
    isNN (N2b a) = a > 3

    eq (N2b a) b = b == c2w 'Q' || b == c2w 'q' ||
                   "TCAG" `B.index` fromIntegral a == b ||
                   "tcag" `B.index` fromIntegral a == b

    generic !ref !smp = case view ref of
            NilRef                     -> lift $ S.effects smp
            l :== ref'                 -> ns l ref' (S.drop (fromIntegral l) smp)
            u :^  ref' | isNN u        -> ns 1 ref' (S.drop               1  smp)
            u :^  ref' ->
              lift (S.nextByte smp) >>= \case
                Left r                 -> pure r
                Right (x, smp')
                    | x .&. 0x7F <= 32 -> generic ref  smp'
                    | isN  x           -> ns    1 ref' smp'
                    | eq u x           -> eqs_2 1 ref' smp'
                    | otherwise        -> encVar u x `Q.cons` generic ref' smp'

    eqs_2 !n !ref !smp = case view ref of
            NilRef                     -> Eqs2 n `Q.cons` lift (S.effects smp)
            l :== ref'                 -> Eqs2 n `Q.cons` ns l ref' (S.drop (fromIntegral l) smp)
            u :^  ref' | isNN u        -> Eqs2 n `Q.cons` ns 1 ref' (S.drop               1  smp)
            u :^  ref'                 -> lift (S.nextByte smp) >>= \case
                Left r                 -> Q.cons (Eqs2 n) (pure r)
                Right (x, smp')
                    | x .&. 0x7F <= 32 -> eqs_2       n  ref  smp'
                    | eq u x           -> eqs_2 (succ n) ref' smp'
                    | isN  x           -> Q.cons (Eqs2 n) $ ns 1 ref' smp'
                    | otherwise        -> Q.cons (Eqs2 n) $ Q.cons (encVar u x) $ generic ref' smp'

    ns !n !ref !smp = case view ref of
            NilRef                     -> Ns n `Q.cons` lift (S.effects smp)
            l :== ref'                 -> ns (l+n) ref' (S.drop (fromIntegral l) smp)
            u :^  ref' | isNN u        -> ns (1+n) ref' (S.drop               1  smp)
            u :^  ref' ->
              lift (S.nextByte smp) >>= \case
                Left r                 -> Ns n `Q.cons` pure r
                Right (x, smp')
                    | x .&. 0x7F <= 32 -> ns       n  ref  smp'
                    | isN  x           -> ns (succ n) ref' smp'
                    | eq u x           -> Ns n `Q.cons` eqs_2 1 ref' smp'
                    | otherwise        -> Q.cons (Ns n) $ Q.cons (encVar u x) $ generic ref' smp'


-- two alleles in bits 0,1 and 2,3
fromAmbCode :: Word8 -> Word8
fromAmbCode c | c == c2w 't' =  0
              | c == c2w 'c' =  5
              | c == c2w 'a' = 10
              | c == c2w 'g' = 15

              | c == c2w 's' =  7
              | c == c2w 'w' =  2
              | c == c2w 'm' =  6
              | c == c2w 'k' =  3
              | c == c2w 'r' = 11
              | c == c2w 'y' =  1
              | otherwise    = throw $ DataError Nothing ("fromAmbCode " ++ show c)

encVar :: Nuc2b -> Word8 -> Lump
encVar r c = encTwoNucs r $ fromAmbCode (c .|. 32)

encTwoVars :: Nuc2b -> Nuc2b -> Nuc2b -> Lump
encTwoVars r (N2b v1) (N2b v2) = encTwoNucs r (v1 + 4 * v2)

encTwoNucs :: Nuc2b -> Word8 -> Lump
encTwoNucs (N2b r) ns = fromMaybe (Ns 1) $ vv V.!? fromIntegral (xor ns (r .|. shiftL r 2))
  where
    !vv = V.fromList [ Eqs2 1,    RefTrans,    RefCompl,    RefTCompl
                     , RefTrans,  Trans2,      TransCompl,  TransTCompl
                     , RefCompl,  TransCompl,  Compl2,      ComplTCompl
                     , RefTCompl, TransTCompl, ComplTCompl, TCompl2 ]

encOneVar :: Nuc2b -> Nuc2b -> Lump
encOneVar (N2b n0) (N2b v1) = fromMaybe (Ns 1) $ vv V.!? (fromIntegral $ xor n0 v1)
  where
    !vv = V.fromList [ Eqs1 1, Trans1, Compl1, TCompl1
                     , Eqs1 1, Trans1, Compl1, TCompl1
                     , Eqs1 1, Trans1, Compl1, TCompl1
                     , Eqs1 1, Trans1, Compl1, TCompl1 ]

diff2 :: Monad m => (() -> RefSeq) -> S.ByteStream m r -> Stream (Of Lump) m r
diff2 = gendiff viewRS . ($ ())

-- XXX  This removes gaps from the reference string.  Ugly and
-- undesirable, but hard to do right at the moment...
diff :: Monad m => B.ByteString -> B.ByteString -> Stream (Of Lump) m ()
diff s1 s2 = gendiff viewBS s1' $ S.chunk s2'
  where
    viewBS = maybe NilRef (\(a,b) -> fromCode (a .|. 32) :^ b) . B.uncons

    fromCode c | c == c2w 't' = N2b 0
               | c == c2w 'c' = N2b 1
               | c == c2w 'a' = N2b 2
               | c == c2w 'g' = N2b 3
               | otherwise =  N2b 255

    s1' = B.filter (/= c2w '-') s1
    s2' = B.pack . map snd . filter ((/= c2w '-') . fst) $ B.zip s1 s2

data Frag a = Short !Char a | Long !L.ByteString a
  deriving Functor

patch :: Monad m => RefSeq -> Stream (Of Lump) m r -> Stream Frag m (Stream (Of Lump) m r)
patch ref = case unconsRS ref of
    Just (N2b hd,tl) -> lift . Q.next >=> \case
        Left  r ->          yields $ Long (unpackRS ref) (pure r)
        Right l -> case l of
            (Break   ,s) -> yields $ Long (L.replicate (lengthRS ref) 'N') s

            (Eqs2   n,s) -> wrap $ Long          (unpackRS $ takeRS n ref) $ patch (dropRS n ref) s
            (Eqs1   n,s) -> wrap $ Long          (unpackRS $ takeRS n ref) $ patch (dropRS n ref) s
            (Ns     n,s) -> wrap $ Long (L.replicate (fromIntegral n) 'N') $ patch (dropRS n ref) s

            (Del1   n,s) -> wrap $ Long (L.replicate (fromIntegral n) '-') $ patch (dropRS n ref) s
            (Del2   n,s) -> wrap $ Long (L.replicate (fromIntegral n) '-') $ patch (dropRS n ref) s
            (DelH   n,s) -> wrap $ Long          (unpackRS $ takeRS n ref) $ patch (dropRS n ref) s

            (Ins1   _,s) -> patch ref s
            (Ins2   _,s) -> patch ref s
            (InsH   _,s) -> patch ref s

            (Trans1     ,s) -> step "CTGA" s
            (Trans2     ,s) -> step "CTGA" s
            (Compl1     ,s) -> step "AGTC" s
            (Compl2     ,s) -> step "AGTC" s
            (TCompl1    ,s) -> step "GACT" s
            (TCompl2    ,s) -> step "GACT" s

            (RefTrans   ,s) -> step "YYRR" s
            (RefCompl   ,s) -> step "WSWS" s
            (TransCompl ,s) -> step "MKKM" s
            (RefTCompl  ,s) -> step "KMMK" s
            (TransTCompl,s) -> step "SWSW" s
            (ComplTCompl,s) -> step "RRYY" s
          where
            step cs s = wrap $ Short (if hd > 3 then 'N' else w2c $ B.index cs (fromIntegral hd)) (patch tl s)

    Nothing      -> clear
      where
        clear :: Monad m => Stream (Of Lump) m r -> Stream Frag m (Stream (Of Lump) m r)
        clear = lift . Q.next >=> \case
            Left          r  -> pure (pure r)
            Right (Break, a) -> pure a
            Right (_,     a) -> clear a


