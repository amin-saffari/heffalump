{-# LANGUAGE CPP, Rank2Types, ExistentialQuantification #-}
module Query ( main_query ) where

{- | About Format Strings

Instead of separate format strings, I now interpolate all string
literals.  An expression that doesn't parse properly is parsed again as
a string literal with implied outermost quotes.  I use @%@ to introduce
interpolation, so that @$@ is left for the shell.  In addition to the
usual C escapes and a single unicode escape of the form
@\\u{hex-digits}@, @\\@ can also escape anything else, including @%@,
@{@, @}@, @[@, @]@ and @|@.

- Interpolation of atoms and parenthesized expressions is part of
  'p_expr' now, it recurses exactly the same way as ordinary
  parenthesized expressions.

- The filter expression is parsed as @p_expr p_inner_atom@.
  'p_inner_atom' allows the fences @{| |}@ and @[| |]@, which in turn
  allow only 'p_prim_atom'.  No accumulation through @{ }@ is accepted.

- The '--format' option can parse as either a @p_expr p_inner_atom@, the
  result of which is formatted using 'showValue', or an interpolated
  string.  The format string is (roughly) @interpolatedString
  p_inner_atom@, so interpolation of expressions works, but accumulation
  is rejected.  It allows fences around a format string, but these are
  not nestable.  This is done by recursing into (roughly)
  @interpolatedString (p_term p_inner_atom)@.

- The accumulator expression is either @p_expr p_outer_atom@ or an
  interpolated string.  The fences @{| |}@ and @[| |]@ cannot be nested,
  and accumulation @{ }@ cannot be nested, but they can be nested inside
  each other once, in either order.  Parsing it as a format string works
  in roughly the same way.
-}

import Bio.Prelude
import Eigenstrat                               ( writeEigenstrat, writeAncestrymap )
import Genome
import Lump
import ShameMS
import Streaming                                ( Stream, Of(..), MonadTrans(..), Compose(..), maps, yields )
import System.Console.GetOpt
import Text.Parsec                              ( (<?>) )
import Util
import Value
import VcfOut                                   ( writeVcf )

import qualified ByteStream                     as S
import qualified Data.ByteString.Builder        as B
import qualified Data.ByteString.Char8          as C
import qualified Data.HashMap.Strict            as M
import qualified Data.IntervalMap.FingerTree    as I
import qualified Data.Vector                    as V
import qualified Data.Vector.Storable           as U
import qualified Streaming                      as Q
import qualified Streaming.Prelude              as Q
import qualified Text.Parsec                    as P
import qualified Text.Parsec.Error              as P
import qualified Text.Parsec.Expr               as P

-- Environment used in the compilation of expressions.
data Env = Env { env_refs    :: V.Vector Bytes
               , env_indivs  :: Bytes -> Bytes
               , env_samples :: V.Vector Bytes                                  -- sample names
               , env_pops    :: V.Vector Bytes                                  -- population names
               , env_pop_ixs :: U.Vector Int }                                  -- population indices

data Config = Config
    { conf_expr   :: Func Env Args Bool                                                 -- the filter expression
    , conf_output :: Env -> FilePath -> Stream (Stream (Of Args) IO) IO () -> IO ()
    , conf_filter :: forall r . Stream (Of [Variant]) IO r -> Stream (Of Variant) IO r  -- builtin filters
    , conf_indivs :: Bytes -> Bytes                                                     -- individual-to-population map
    , conf_nout   :: Int
    , conf_blocks :: Maybe Int
    , conf_observ :: Maybe Int
    , conf_ofile  :: FilePath
    , conf_ref    :: Maybe FilePath }

defaultConfig :: Config
defaultConfig = Config
    { conf_expr   = pure True                           -- everything counts
    , conf_output = \_ _ -> Q.effects . Q.concats       -- output nothing
    , conf_filter = streamConcat                        -- even split variants
    , conf_ofile  = "-"                                 -- to stdout (if appropriate)
    , conf_indivs = id                                  -- everyone is a singleton pop
    , conf_nout   = 0                                   -- no outgroups
    , conf_blocks = Nothing                             -- one big block
    , conf_observ = Nothing
    , conf_ref    = Nothing }                           -- no explicit reference

options :: [ OptDescr (Config -> IO Config) ]
options =
    [ Option "e" ["expr","filter"]  (ReqArg set_expr   "EXPR") "Set EXPR as filter"
    , Option "f" ["format"]         (ReqArg set_format "EXPR") "Evaluate and print EXPR for each variant"
    , Option "a" ["accum"]          (ReqArg set_accum  "EXPR") "Evaluate, summarize, print EXPR for each block"
    , Option "n" ["numoutgroups"]   (ReqArg set_nout    "NUM") "The first NUM individuals are outgroups (0)"
    , Option "b" ["only-biallelic"] (NoArg  set_no_split     ) "Discard, don't split, polyallelic sites"
    , Option "l" ["block-length"]   (ReqArg set_lblock  "LEN") "Blocks of maximum length LEN"
    , Option "m" ["num-observed"]   (ReqArg set_tblock  "NUM") "Number of observed sites per block"
    , Option "r" ["reference"]      (ReqArg set_ref    "FILE") "Read reference from FILE (.2bit)"
    , Option "i" ["individuals"]    (ReqArg set_indiv  "FILE") "Read individuals from FILE (.ind)"
    , Option "o" ["output"]         (ReqArg set_ofile  "FILE") "Write output to FILE"
    , Option [ ] ["treemix"]        (NoArg  set_treemix      ) "Write treemix format"
    , Option [ ] ["eigenstrat"]     (NoArg  set_eigenstrat   ) "Write eigenstrat format"
    , Option [ ] ["ancestrymap"]    (NoArg  set_ancestrymp   ) "Write ancestrymap format"
    , Option [ ] ["vcf"]            (NoArg  set_vcf          ) "Write vc format"
    , Option [ ] ["pseudo-ms","ms"] (NoArg  set_ms           ) "Writes pseudo-ms format" ]
  where
    set_expr     a c = compile_expr a >>= \f -> return $ c { conf_expr   =            f }
    set_accum    a c = compile_acc  a >>= \f -> return $ c { conf_output =            f }
    set_format   a c = compile_fmt  a >>= \f -> return $ c { conf_output =            f }
    set_nout     a c = readIO       a >>= \n -> return $ c { conf_nout   =            n }
    set_lblock   a c = readIO       a >>= \n -> return $ c { conf_blocks =       Just n }
    set_tblock   a c = readIO       a >>= \n -> return $ c { conf_observ =       Just n }
    set_no_split   c =                          return $ c { conf_filter =  singlesOnly }
    set_ref      a c =                          return $ c { conf_ref    =       Just a }
    set_indiv    a c = read_indivs  a >>= \f -> return $ c { conf_indivs =            f }
    set_ofile    f c =                          return $ c { conf_ofile  =            f }
    set_treemix    c =                          return $ c { conf_output =  output_tmix }
    set_eigenstrat c =                          return $ c { conf_output = output_eigen }
    set_ancestrymp c =                          return $ c { conf_output = output_ancmp }
    set_vcf        c =                          return $ c { conf_output =   output_vcf }
    set_ms         c =                          return $ c { conf_output = output_shame }


main_query :: [String] -> IO ()
main_query args = do
    ( hefs, Config{..} ) <- parseFile1Opts defaultConfig "query" "[hef-file...]" options args

    decodeManyRef conf_ref hefs $ \refs inps -> do
            let env_samples = V.fromList $ map fpToSampleName hefs
                (env_pop_ixs, env_pops) = mk_popmap conf_indivs env_samples
                env_refs = V.fromList $ rss_chroms refs
                env_indivs = conf_indivs

            conf_output Env{..} conf_ofile $
                Q.maps ( Q.filter (getFunc conf_expr Env{..})
                       . Q.map (mk_args Env{..} None)
                       . conf_filter
                       . expandVars conf_nout ) $
                do_blocks conf_observ conf_blocks $
                addRef (MaxHole 49) refs $
                mergeLumps conf_nout inps
  where
    do_blocks        _   Nothing  = yields
    do_blocks  Nothing  (Just  l) = physicalBlocks l
    do_blocks (Just  m) (Just  l) = observedBlocks (MinObserved m) (MaxPhysical l)


-- map each individual to population number(!), make list of populations
mk_popmap :: Foldable t => (Bytes -> Bytes) -> t Bytes -> (U.Vector Int, V.Vector Bytes)
mk_popmap i2p = (U.fromList . reverse *** V.fromList) . foldl' (uncurry go) ([], [])
  where
    go is ps x = case elemIndex (i2p x) ps of
        Just i -> (i:is, ps)
        Nothing -> (length is : is, i2p x : ps)

mk_args :: Env -> Index -> Variant -> Args
mk_args env idx var =
    Args idx var $!
        U.accumulate_ addAC
            (U.replicate (V.length $ env_pops env) (I2 0 0))
            (U.generate (U.length $ v_calls var) ((U.!) (env_pop_ixs env)))
            (v_calls var)


read_indivs :: FilePath -> IO (Bytes -> Bytes)
read_indivs fp = withInputFile fp $ Q.fold_ get1 M.empty (\m k -> M.lookupDefault k k m) . S.lines' . S.hGetContents
  where
    get1 m s = case C.words s of x:_:y:_ -> M.insert x y m ; _ -> m


showErr :: String -> String -> String -> P.ParseError -> String
showErr f m s e =
        printf "\nerror parsing %s as %s at column %d near `%s`:"
                f m (P.sourceColumn $ P.errorPos e) (take 32 $ drop (P.sourceColumn (P.errorPos e)) s)
        ++ P.showErrorMessages "or" "unknown parse error"
                               "expecting" "unexpected" "end of input"
                               (P.errorMessages e)


compile_expr :: String -> IO (Func Env Args Bool)
compile_expr s = either (throwIO . PebkacError . showErr "--expr" "expression" s) (return . fmap to_bool)
                 =<< P.runParserT (P.spaces *> p_expr p_inner_atom <* P.eof) () "" s

-- An accumulating expression is an expression built from 'Fold's and
-- hence resulting in 'Folds'.  Its atoms could be fenced expressions,
-- where the fence promotes a function to a 'Fold', or ordinary atoms.
compile_acc :: String -> IO (Env -> FilePath -> Stream (Stream (Of Args) IO) IO r -> IO r)
compile_acc s = P.runParserT (P.spaces *> p_expr p_outer_atom <* P.eof) () "" s >>= \case
                    Right expr -> return $ to_blocked_output expr
                    Left  err1 ->
                        P.runParserT (p_format <* P.eof) () "" s >>= \case
                            Right fmt -> return $ to_blocked_output fmt
                            Left err2 ->
                                throwIO . PebkacError $
                                    showErr "--accum" "expression" s err1 ++ " or " ++
                                    showErr "--accum" "interpolated string" s err2
  where
    p_format = fmap liftInterp . interpolatedString $ P.choice
                    [ P.char '%'   *> p_term      p_outer_atom
                    , map_samples <$> barbed_e    p_format_inner
                    , map_popns   <$> enveloped_e p_format_inner ]

    p_format_inner :: Parser (EFold Args Value)
    p_format_inner = liftInterp <$> interpolatedString (P.char '%' *> p_term p_outer_atom)

    -- Allows envelopes (\"[| |]\") to map over samples and barbed wire
    -- (\"{| |}\") to map over populations.  Also allows accumulation.
    -- These cannot be nested.
    p_outer_atom :: Parser Expr
    p_outer_atom = P.choice [ map_samples   <$> enveloped (p_expr p_simple_outer_atom)
                            , map_popns     <$> barbed    (p_expr p_simple_outer_atom)
                            ,                                     p_fold_atom p_inner_atom
                            , const_to_fold <$>                   p_const_atom ]

    -- Allows accumulation, but no fences.
    p_simple_outer_atom = P.choice [                              p_fold_atom p_prim_atom
                                   , const_to_fold <$>            p_const_atom ]

    const_to_fold :: CFun Env a -> EFold Args a
    const_to_fold (CFun _c f) = Compose $ pure . f

    to_blocked_output f env fp strm =
        case getCompose f env of
            Fold comb step ini fin ->
                let stream_fold :: Stream (Of Args) IO r2 -> IO (Of Value r2)
                    stream_fold = Q.next >=> \case
                        Left r -> return (fin ini :> r)
                        Right (x,q) -> Q.fold (\a -> comb a . step) (step x) fin q

                in withOutputFile fp $ \h ->
                    S.hPut h . S.concat .
                    maps (lift . stream_fold >=> \(x :> r) -> r <$ S.toByteStream (showValue x)) $ strm



compile_fmt :: String -> IO (Env -> FilePath -> Stream (Stream (Of Args) IO) IO r -> IO r)
compile_fmt s = P.runParserT (P.spaces *> p_expr p_inner_atom <* P.eof) () "" s >>= \case
                    Right expr -> return $ to_output expr
                    Left  err1 ->
                        P.runParserT (p_format <* P.eof) () "" s >>= \case
                            Right fmt -> return $ to_output fmt
                            Left err2 ->
                                throwIO . PebkacError $
                                    showErr "--format" "expression" s err1 ++ " or " ++
                                    showErr "--format" "interpolated string" s err2
  where
    p_format = fmap liftInterp . interpolatedString $ P.choice
                    [ P.char '%'         *> p_term      p_inner_atom
                    , map_samples_inner <$> barbed_e    p_format_inner
                    , map_popns_inner   <$> enveloped_e p_format_inner ]

    p_format_inner = liftInterp <$> interpolatedString (P.char '%' *> p_term p_inner_atom)

    to_output f env fp q =
        withOutputFile fp $ \h ->
            S.hPut h . S.concat $ maps (\(x :> r) ->
                r <$ S.toByteStream (showValue (getFunc f env x))) $ Q.concats q


data Index = Indiv Int | Pop Int | None
data Args  = Args { ind :: Index, var :: Variant, cts :: U.Vector I2 }

type Parser = P.ParsecT String () IO
type EFold a = Compose ((->) Env) (Fold a)
type Expr    = EFold Args Value

map_samples, map_popns :: EFold Args Value -> EFold Args Value
map_samples f = Compose $ \e -> case getCompose f e of
                    Fold comb step ini fin -> Fold (V.zipWith comb) steps inis fins
                      where steps     a = V.imap (\i _ -> step a { ind = Indiv i }) (env_samples e)
                            inis        = V.map (const ini) (env_samples e)
                            fins        = accum_values (fin ini) . V.map fin

map_popns f = Compose $ \e -> case getCompose f e of
                    Fold comb step ini fin -> Fold (V.zipWith comb) steps inis fins
                      where steps     a = V.imap (\i _ -> step a { ind = Pop i }) (env_pops e)
                            inis        = V.map (const ini) (env_pops e)
                            fins        = accum_values (fin ini) . V.map fin

map_samples_inner, map_popns_inner :: Func Env Args Value -> Func Env Args Value
map_samples_inner (Func f g) = Func f (\e x -> accum_values f $ V.imap (\i _ -> g e x { ind = Indiv i }) (env_samples e))
map_popns_inner   (Func f g) = Func f (\e x -> accum_values f $ V.imap (\i _ -> g e x { ind = Pop   i }) (env_pops    e))


enveloped, barbed :: Parser a -> Parser a
enveloped p = P.try (P.char '[' *> P.char '|') *> P.spaces *> p <* P.char '|' <* P.char ']' <* P.spaces
barbed    p = P.try (P.char '{' *> P.char '|') *> P.spaces *> p <* P.char '|' <* P.char '}' <* P.spaces

-- same as barbed/enveloped, but doesn't eat whitespace
enveloped_e, barbed_e :: Parser a -> Parser a
enveloped_e p = P.try (P.char '[' *> P.char '|') *> p <* P.char '|' <* P.char ']'
barbed_e    p = P.try (P.char '{' *> P.char '|') *> p <* P.char '|' <* P.char '}'


p_prim_atom :: Parser (Func Env Args Value)
p_prim_atom = const_to_func <$> p_const_atom <|> p_func_atom

-- Allows envelopes (\"[| |]\") to map over samples and barbed wire
-- (\"{| |}\") to map over populations.  These cannot be nested.
p_inner_atom :: Parser (Func Env Args Value)
p_inner_atom = P.choice [ map_samples_inner <$> enveloped (p_expr p_prim_atom)
                        , map_popns_inner   <$> barbed    (p_expr p_prim_atom)
                        ,                                         p_prim_atom ]

p_const_atom :: Parser (CFun Env Value)
p_const_atom = P.choice
    [ CFun (Int 0) . const                        <$> naturalOrFloat
    , CFun (Int 0) (Int . V.length . env_pops)    <$  reserved "n_pops"
    , CFun (Int 0) (Int . V.length . env_samples) <$  reserved "n_samples" ]

p_fold_atom :: Parser (Func Env Args Value) -> Parser Expr
p_fold_atom p_nested = func_to_fold <$ P.char '{' <* P.spaces <*> p_expr p_nested <* P.char '}'
  where
    func_to_fold :: Func Env Args Value -> EFold Args Value
    func_to_fold (Func c f) = Compose $ \e -> Fold plus_values (f e) c id


p_func_atom :: Parser (Func Env Args Value)
p_func_atom = P.choice
    [ int (const $ v_chr . var)                                         <$  reserved "rnum"
    , int (const $ v_pos . var)                                         <$  reserved "pos"
    , string (\e -> at (env_refs e) . v_chr . var)                      <$  reserved "rname"
    , string (const $ C.singleton .        toRefCode .     v_ref . var) <$  reserved "ref"
    , string (const $ C.singleton . liftA2 toAltCode v_alt v_ref . var) <$  reserved "alt"

    , string . get_sample_name    <$ reserved "sample"  <*> subscript
    , string . get_pop_name       <$ reserved "pop"     <*> subscript
    , string . get_gt      fmt_gt <$ reserved "gt"      <*> subscript
    , string . get_gt     fmt_tgt <$ reserved "tgt"     <*> subscript
    , string . get_gt     fmt_gt1 <$ reserved "gt1"     <*> subscript
    , string . get_gt    fmt_tgt1 <$ reserved "tgt1"    <*> subscript
    , string . get_gt     fmt_gt2 <$ reserved "gt2"     <*> subscript
    , string . get_gt    fmt_tgt2 <$ reserved "tgt2"    <*> subscript
    , string . get_gt fmt_iupacgt <$ reserved "iupacgt" <*> subscript
    , int    . get_ac         fst <$ reserved "an"      <*> subscript
    , int    . get_ac         snd <$ reserved "ac"      <*> subscript

    -- XXX  these need to change once indels are actually supported
    , boolv (const $ not . isTransversion . v_alt . var) <$ reserved "is_transition"
    , boolv (const $       isTransversion . v_alt . var) <$ reserved "is_transversion"
    , int (const $ const 0)                              <$ reserved "is_insertion"
    , int (const $ const 0)                              <$ reserved "is_deletion"

    , reserved "anno" >> stringLiteral >>= lookup_bed ]
  where
    at :: V.Vector Bytes -> Int -> Bytes
    at v i = fromMaybe C.empty $ v V.!? i

    subscript :: Parser (Env -> Args -> Either Index Value)
    subscript = P.option (const $ Left . ind) (bracketed proper_subscript)

    proper_subscript = P.choice [ ((Right .) .) . getFunc   <$> p_expr p_prim_atom
                                , const (const (Left None)) <$  P.char '*' <* P.spaces
                                ] <?> "subscript"

    bracketed p = P.try (P.spaces *> P.char '[') *> P.spaces *> p <* P.char ']'

    get_sample_name arg env args  =
        case arg env args of
            Right (Int    i)                             -> env_samples env `at` i
            Right (Double i)                             -> env_samples env `at` round i
            Right (String w) | Just (i,_) <- C.readInt w -> env_samples env `at` i

            Left  (Indiv i)                              -> env_samples env `at` i
            Left   None                                  -> C.singleton '*'
            _                                            -> C.empty

    get_pop_name arg env args =
        case arg env args of
            Right (Int    i) -> xlate i
            Right (Double i) -> xlate (round i)
            Right (String w) -> env_indivs env w
            Right  _         -> C.empty

            Left  (Indiv i)  -> xlate i
            Left  (Pop   i)  -> env_pops env `at` i
            Left   None      -> C.singleton '*'
      where
        xlate i = maybe C.empty (env_indivs env) (env_samples env V.!? i)

    get_gt f arg env args =
        case arg env args of
            Right (Int    i) -> xlate i
            Right (Double i) -> xlate (round i)
            Right (String w) -> env_indivs env w
            Left  (Indiv i)  -> xlate i
            _                -> C.empty
      where
        xlate i = maybe C.empty (f (var args)) $ v_calls (var args) U.!? i

    fmt_gt _ (AC 0 0) = "."
    fmt_gt _ (AC 1 0) = "0"
    fmt_gt _ (AC 0 1) = "1"
    fmt_gt _ (AC _ 0) = "0/0"
    fmt_gt _ (AC 0 _) = "1/1"
    fmt_gt _ (AC _ _) = "0/1"

    fmt_gt1 _ (AC 0 0) = "."
    fmt_gt1 _ (AC 0 _) = "1"
    fmt_gt1 _ (AC _ _) = "0"

    fmt_gt2 _ (AC 0 0) = "."
    fmt_gt2 _ (AC _ 0) = "0"
    fmt_gt2 _ (AC _ _) = "1"

    with_tgt f v = fromString . f r a
      where !r = toRefCode           (v_ref v)
            !a = toAltCode (v_alt v) (v_ref v)

    fmt_tgt = with_tgt $ \r a -> \case
            AC 0 0 -> ['N']
            AC 1 0 -> [r]
            AC 0 1 -> [a]
            AC _ 0 -> [r,'/',r]
            AC 0 _ -> [a,'/',a]
            AC _ _ -> [r,'/',a]

    fmt_tgt1 = with_tgt $ \r a -> \case
            AC 0 0 -> ['N']
            AC 0 _ -> [a]
            AC _ _ -> [r]

    fmt_tgt2 = with_tgt $ \r a -> \case
            AC 0 0 -> ['N']
            AC _ 0 -> [r]
            AC _ _ -> [a]

    fmt_iupacgt v = C.singleton . \case
            AC 0 0 -> 'N'
            AC _ 0 -> toRefCode             (v_ref v)
            AC 0 _ -> toAltCode   (v_alt v) (v_ref v)
            AC _ _ -> toIupacCode (v_alt v) (v_ref v)

    toIupacCode :: Var2b -> Nuc2b -> Char
    toIupacCode (V2b v) (N2b r)
        | v .|. r >= 4  = 'N'
        | otherwise     = C.index "TCAGYYRRWSWSKMMK" . fromIntegral $ 4 * v + r


    -- pass two counts to 'f':  total count first, alt count second
    get_ac f arg env a@Args{..} =
        case arg env a of
            Right (Int    i) -> get_ind i
            Right (Double i) -> get_ind (round i)
            Right (String w) -> maybe 0 get_ind $ V.elemIndex w (env_samples env)
            Right  Null      -> 0

            Left  (Indiv i)  -> get_ind i
            Left  (Pop   i)  -> get_pop i
            Left   None      -> f . from_i2 $ U.foldl' (\(I2 x y) (I2 u v) -> I2 (x+u) (y+v)) (I2 0 0) cts
      where
        from_ac :: AlleleCounts -> (Int, Int)
        from_ac (AC x y) = (fromIntegral (x+y), fromIntegral y)

        from_i2 :: I2 -> (Int, Int)
        from_i2 (I2 x y) = (fromIntegral (x+y), fromIntegral y)

        get_ind i = maybe 0 (f . from_ac) $ v_calls var U.!? i
        get_pop i = maybe 0 (f . from_i2) $ cts U.!? i


p_term :: Applicative f => Parser (f Value) -> Parser (f Value)
p_term p_prim = P.char '(' *> P.spaces *> p_expr p_prim <* P.char ')' <|> p_prim <?> "simple expression"

p_expr :: Applicative f => Parser (f Value) -> Parser (f Value)
p_expr p_prim = P.buildExpressionParser optable ((interpol <|> p_term p_prim) <* P.spaces) <?> "expression"
  where
    interpol = liftInterp <$> quotedString (P.char '%' >> p_term p_prim)

    optable = [ [ prefix  notV "!",  prefix  negV "-"  ]
              , [ binaryL mulV "*",  binaryL divV "/"  ]
              , [ binaryL addV "+",  binaryL subV "-"  ]
              , [ binaryL catV ".",  binaryL sepV "," ]
              , [ binaryB (==) "==", binaryB (<=) "<=", binaryB (<) "<"
                , binaryB (/=) "!=", binaryB (>=) ">=", binaryB (>) ">"
                , binaryN matV "=~", binaryN nmtV "!~" ]
              , [ binaryL andV "&&" ]
              , [ binaryL orV  "||" ] ]

    prefix  f s = P.Prefix (liftA  f  <$ operator s)
    binaryN f s = P.Infix  (liftA2 f  <$ operator s) P.AssocNone
    binaryB f s = P.Infix  (liftA2 f' <$ operator s) P.AssocNone where f' = (.) from_bool . f
    binaryL f s = P.Infix  (liftA2 f  <$ operator s) P.AssocLeft


operator :: String -> Parser ()
operator nm = P.try (do cs <- P.many1 (P.oneOf ":!#$%&*+.,/<=>?@\\^|-~")
                        guard $ cs == nm
                        P.spaces)
              <?> show nm

reserved :: String -> Parser ()
reserved nm = P.try (do c <- P.letter
                        cs <- P.many (P.alphaNum <|> P.char '_')
                        guard $ map toLower (c:cs) == map toLower nm)
              <?> show nm

hex :: Parser Int
hex = foldl' (\a c -> 16*a + digitToInt c) 0 <$> P.many1 P.hexDigit <?> "hexadecimal number"

naturalOrFloat :: Parser Value
naturalOrFloat = ( reads <$> P.getInput >>= \case
                        [(i,t)] | null t || head t `notElem` (".eE" :: String) -> Int i <$ P.setInput t
                        _                                                      -> empty )
             <|> ( reads <$> P.getInput >>= \case
                        [(d,t)] -> Double d <$ P.setInput t
                        _       -> empty )
             <?> "number"

stringLiteral :: Parser String
stringLiteral = foldMap (either (\() -> "") id) <$> quotedString empty

liftInterp :: Applicative f => [Either (f Value) String] -> f Value
liftInterp = foldr (liftA2 plus_values . either id (pure . String . fromString)) (pure $ String C.empty)

quotedString :: Parser a -> Parser [Either a String]
quotedString p = P.between (P.char '"')
                           (P.char '"' <?> "end of string")
                           (interpolatedString p)
                 <?> "literal string"

interpolatedString :: Parser a -> Parser [Either a String]
interpolatedString p = collect <$> P.many stringPrim
  where
    stringLetter = P.satisfy $ \c -> c /= '"' && c /= '|' && c > '\026'
    stringChar   = (P.char '\\' >> stringLetter >>= fromEscape) <|> stringLetter
    stringPrim   = P.choice [ Left  <$> p          <?> "string interpolation"
                            , Right <$> stringChar <?> "string character"     ]


    fromEscape 'a' = pure '\a'
    fromEscape 'b' = pure '\b'
    fromEscape 'f' = pure '\f'
    fromEscape 'n' = pure '\n'
    fromEscape 'r' = pure '\r'
    fromEscape 't' = pure '\t'
    fromEscape 'v' = pure '\v'
    fromEscape 'u' = chr <$> P.between (P.char '{') (P.char '}') hex
    fromEscape  c  = pure c

    collect (Left  a : as) = Left a : collect as
    collect (Right c : as) = collect' [c] as
    collect [            ] = []

    collect' cs (Right c : as) = collect' (c:cs) as
    collect' cs            as  = Right (reverse cs) : collect as


type Anno = HashMap Bytes (I.IntervalMap Int Bytes)

lookup_bed :: FilePath -> Parser (Func Env Args Value)
lookup_bed fn = do
    anno <- lift $ withInputFile fn $ Q.fold_ parse1 M.empty id . S.lines' . S.decomp . S.hGetContents

    return $ Func Null $ \env Args{..} ->
        let chrm = env_refs env V.! v_chr var
            imap = M.lookupDefault I.empty chrm anno
            -- XXX this will break for indels
            annos = map snd $ I.search (v_pos var) imap
        in
            if null annos then Null else String $ C.intercalate "\t" annos
  where
    parse1 :: Anno -> C.ByteString -> Anno
    parse1 m ln =
            fromMaybe m $ do
                   sq:frm:tho:nm:_ <- Just $ C.words ln
                   (start,"")      <- C.readInt frm
                   (end,  "")      <- C.readInt tho
                   let iv = I.singleton (I.Interval start (end-1)) (C.copy nm)
                   return $! M.insertWith I.union (C.copy sq) iv m


-- cloned from src/Treemix, because it simplifies considerably
output_tmix :: Env -> FilePath -> Stream (Stream (Of Args) IO) IO r0 -> IO r0
output_tmix Env{..} fp s =
    withOutputFile fp $ \hdl ->
      S.hPut hdl $ S.gzip $ do
        S.toByteStream (V.foldr (\a k -> B.byteString a <> B.char7 ' ' <> k) (B.char7 '\n') env_pops)
        S.concat $ Q.maps (\(Args{..} :> r) -> r <$ S.toByteStream (U.foldr ((<>) . show1) (B.char7 '\n') cts))
                 $ Q.concats s
  where
    show1 (I2 a b) = B.intDec a <> B.char7 ',' <> B.intDec b <> B.char7 ' '

output_eigen :: Env -> FilePath -> Stream (Stream (Of Args) IO) IO r0 -> IO r0
output_eigen Env{..} fp = writeEigenstrat fp (V.toList env_samples) . Q.map var . Q.concats

output_ancmp :: Env -> FilePath -> Stream (Stream (Of Args) IO) IO r0 -> IO r0
output_ancmp Env{..} fp = writeAncestrymap fp (V.toList env_samples) . Q.map var . Q.concats

output_vcf :: Env -> FilePath -> Stream (Stream (Of Args) IO) IO r0 -> IO r0
output_vcf Env{..} fp s =
    withOutputFile fp $ \hdl ->
        writeVcf env_refs (V.toList env_samples) hdl $ Q.map var $ Q.concats s

output_shame :: Env -> FilePath -> Stream (Stream (Of Args) IO) IO r0 -> IO r0
output_shame Env{..} fp q =
    withOutputFile fp $ \hdl ->
        Q.mapsM_ ( (>>) (hPutStrLn hdl "\n//\n")
                 .  blockToShame hdl . Q.map var ) q

