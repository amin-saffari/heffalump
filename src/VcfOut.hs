module VcfOut ( main_vcfout, writeVcf ) where

import Bio.Prelude
import System.Console.GetOpt

import qualified Data.ByteString.Builder         as B
import qualified Data.Vector                     as V
import qualified Data.Vector.Storable            as U
import qualified Streaming.Prelude               as Q

import Bed ( readBed, filterWithBed )
import Genome
import Lump
import Util ( parseFile1Opts, streamConcat, singlesOnly )

data ConfVcfOut = ConfVcfOut {
    conf_noutgroups :: Maybe Int,
    conf_all        :: Bool,
    conf_split      :: Bool,
    conf_reference  :: Maybe FilePath,
    conf_regions    :: Maybe FilePath }
  deriving Show

defaultVcfConf :: ConfVcfOut
defaultVcfConf = ConfVcfOut (Just 0) True True Nothing Nothing

opts_vcfout :: [ OptDescr (ConfVcfOut -> IO ConfVcfOut) ]
opts_vcfout =
    [ Option "r" ["reference"]     (ReqArg set_ref "FILE") "Read reference from FILE (.2bit)"
    , Option "n" ["numoutgroups"]  (ReqArg set_nout "NUM") "The first NUM individuals are outgroups (0)"
    , Option "D" ["dense"]               (NoArg set_dense) "Output invariant sites, too"
    , Option "t" ["only-transversions"] (NoArg set_no_all) "Output only transversion sites"
    , Option "b" ["only-biallelic"]   (NoArg set_no_split) "Discard, don't split, polyallelic sites"
    , Option "R" ["regions"]      (ReqArg set_rgns "FILE") "Restrict to regions in bed-file FILE" ]
  where
    set_ref    a c = return $ c { conf_reference  =  Just a }
    set_nout   a c = (\n ->   c { conf_noutgroups =  Just n }) <$> readIO a
    set_dense    c = return $ c { conf_noutgroups = Nothing }
    set_no_all   c = return $ c { conf_all        =   False }
    set_no_split c = return $ c { conf_split      =   False }
    set_rgns   a c = return $ c { conf_regions    =  Just a }


main_vcfout :: [String] -> IO ()
main_vcfout args = do
    ( hefs, ConfVcfOut{..} ) <- parseFile1Opts defaultVcfConf "vcfexport"
                                               "[hef-file...]" opts_vcfout args
    decodeManyRef conf_reference hefs $ \refs inps -> do
        region_filter <- readBed conf_regions (rss_chroms refs)

        writeVcf (V.fromList $ rss_chroms refs) (map fpToSampleName hefs) stdout $
                       filterWithBed region_filter $
                       (if conf_all then id else Q.filter (isTransversion . v_alt)) $
                       bool singlesOnly streamConcat conf_split $
                       maybe expandVarsDense expandVars conf_noutgroups $
                       addRef (MaxHole 49) refs $
                       mergeLumps (fromMaybe 0 conf_noutgroups) inps

writeVcf :: V.Vector Bytes -> [Bytes] -> Handle -> Q.Stream (Q.Of Variant) IO r -> IO r
writeVcf chrs smps hdl vars = do
        B.hPutBuilder hdl $
                   "##fileformat=VCFv4.1\n" <>
                   "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n" <>
                   "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT" <>
                   foldMap ((B.char7 '\t' <>) . B.byteString) smps <>
                   B.char7 '\n'

        flip Q.mapM_ vars $ \Variant{..} ->
                B.hPutBuilder hdl $
                    B.byteString (chrs V.! v_chr) <> B.char8 '\t' <>
                    B.intDec (v_pos+1) <> B.string8 "\t.\t" <>
                    B.char8 (toRefCode v_ref) <> B.char8 '\t' <>
                    B.char8 (toAltCode v_alt v_ref) <> B.string8 "\t.\t.\t.\tGT" <>
                    U.foldr ((<>) . gts) mempty v_calls <>
                    B.char8 '\n'
  where
    gts :: AlleleCounts -> B.Builder
    gts (AC 0 0) = "\t./."      -- N
    gts (AC 1 0) = "\t0"        -- 1x ref
    gts (AC 0 1) = "\t1"        -- 1x alt
    gts (AC _ 0) = "\t0/0"      -- >1x ref
    gts (AC 0 _) = "\t1/1"      -- >1x alt
    gts (AC _ _) = "\t0/1"      -- ref+alt

