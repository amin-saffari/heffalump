{-# LANGUAGE CPP #-}
#if __GLASGOW_HASKELL__ >= 800
{-# OPTIONS_GHC -Wno-orphans #-}
#else
{-# OPTIONS_GHC -fno-warn-orphans #-}
#endif
module Util
    ( streamConcat
    , singlesOnly

    , mk_opts
    , parseOpts
    , parseFileOpts
    , parseFile1Opts
    , parseFasta
    , readSampleFa
    , readNumIO
    , FastaSeq(..)
    , withInputFile
    , withOutputFile

    , PrimMonad(..)
    , vconcats
    , vfroms

    , InternalError(..)
    , DataError(..)
    , PebkacError(..)
    ) where

import Bio.Prelude
import ByteStream                       ( withOutputFile, decomp )
import Control.Monad.Primitive          ( PrimMonad(..) )
import Streaming                        ( Stream, Of(..), MonadTrans(..), inspect, wrap, effect, yields )
import System.Console.GetOpt
import System.IO                        ( withBinaryFile, IOMode(..) )

import qualified ByteStream                      as S
import qualified Data.ByteString                 as B
import qualified Data.Vector.Generic             as V
import qualified Data.Vector.Generic.Mutable     as M
import qualified Streaming.Prelude               as Q

mk_opts :: String -> String -> [OptDescr (b -> IO b)] -> ([OptDescr (b -> IO b)], IO b)
mk_opts cmd moreopts ods = (ods', usage)
  where
    ods' = ods ++ [ Option "h?" ["help","usage"] (NoArg $ const usage) "Display this usage information" ]

    usage = do pn <- getProgName
               hPutStrLn stderr $ usageInfo
                    ("Usage: " ++ pn ++ ' ': cmd ++ " [options...] " ++ moreopts) ods'
               exitSuccess

parseOpts :: a -> ([OptDescr (a -> IO a)], b) -> [String] -> IO a
parseOpts def (ods,_) args = do
    let (opts, files, errs) = getOpt Permute ods args
    unless (null errs) $ mapM_ (hPutStrLn stderr) errs >> exitFailure
    unless (null files) $
        mapM_ (hPutStrLn stderr . (++) "unexpected argument " . show) files >> exitFailure
    foldl (>>=) (return def) opts

parseFileOpts :: a -> String -> String -> [OptDescr (a -> IO a)] -> [String] -> IO ([String], a)
parseFileOpts def lbl more ods args = do
    let ods' = fst $ mk_opts lbl more ods
    let (opts, files, errs) = getOpt Permute ods' args
    unless (null errs) $ mapM_ (hPutStrLn stderr) errs >> exitFailure
    (,) files <$> foldl (>>=) (return def) opts

parseFile1Opts :: a -> String -> String -> [OptDescr (a -> IO a)] -> [String] -> IO ([String], a)
parseFile1Opts def lbl more ods args = do
    let usage = snd $ mk_opts lbl more ods
    (files, a) <- parseFileOpts def lbl more ods args
    when (null files) $ void usage
    return (files, a)

data FastaSeq m r = FastaSeq !B.ByteString (S.ByteStream m r) deriving Functor

readSampleFa :: Handle -> Stream (FastaSeq IO) IO ()
readSampleFa = parseFasta . decomp . S.hGetContents

-- | Parsing a FastA file results in pairs of sequence names and
-- sequences.  The sequences are still text with their case preserved.
-- This assumes that the character '>' does not show up anywhere except
-- to introduce a header and that downstream can deal with whitespace
-- (especially line breaks).  Annoying as the use of
-- streaming-bytestring is here, it completely prevents otherwise
-- unmanageable memory leaks.

parseFasta :: Monad m => S.ByteStream m r -> Stream (FastaSeq m) m r
parseFasta = go . ignoreBody . S.lines
  where
    go :: Monad m => Stream (S.ByteStream m) m r -> Stream (FastaSeq m) m r
    go = lift . inspect >=> go'

    go' :: Monad m => Either r (S.ByteStream m (Stream (S.ByteStream m) m r)) -> Stream (FastaSeq m) m r
    go' (Left    r) = pure r
    go' (Right hdr) = do
                h :> ls <- lift $ S.toStrict hdr
                let name = B.drop 1 $ B.takeWhile (not . is_space_w8) h
                yields (FastaSeq name (S.concat (takeBody ls))) >>= go

    is_space_w8 :: Word8 -> Bool
    is_space_w8 x = (x .&. 0x7F) <= 32

    -- Take lines as long as they don't start with ">"
    takeBody :: Monad m => Stream (S.ByteStream m) m r -> Stream (S.ByteStream m) m (Stream (S.ByteStream m) m r)
    takeBody = lift . inspect >=> \case
        -- empty input stream:  return an empty stream that returns an
        -- empty stream.
        Left r -> pure (pure r)     -- ?!
        -- inspect first line...
        Right line -> lift (S.nextByte line) >>= \case
            -- empty line:  return it and continue
            Left     s'                    -> yields (pure s') >>= takeBody

            -- header line:  return empty stream that returns an empty
            -- stream that starts with the restored line.
            Right (c,line') | c == c2w '>' -> pure (wrap (S.cons c line'))

            -- body line:  return a stream that begins with it, recurse
            -- on its tail
                            | otherwise    -> yields (S.cons c line') >>= takeBody

    ignoreBody :: Monad m => Stream (S.ByteStream m) m r -> Stream (S.ByteStream m) m r
    ignoreBody = lift . inspect >=> \case
        -- empty input stream:  return an empty stream
        Left r -> pure r
        -- inspect first line...
        Right line -> lift (S.nextByte line) >>= \case
            -- empty line:  continue
            Left      s'                   -> ignoreBody s'

            -- header line: return a stream that starts with the restored line.
            Right (c,line') | c == c2w '>' -> wrap (S.cons c line')

            -- body line:  continue
                            | otherwise    -> ignoreBody $ effect (S.effects line')

readNumIO :: String -> IO Int
readNumIO s = case reads s of
    [(n,[ ])] -> return n
    [(n,"k")] -> return $ n * 1000
    [(n,"M")] -> return $ n * 1000000
    [(n,"G")] -> return $ n * 1000000000
    _         -> fail $ "unable to parse: " ++ show s

withInputFile :: FilePath -> (Handle -> IO a) -> IO a
withInputFile "-" k = k stdin
withInputFile  f  k = withBinaryFile f ReadMode k

-- | Concatenates a stream of lists into a stream.  This would be
-- equivalent to 'Q.concat' if 'Q.concat' didn't have a performance bug
-- that made it completely unusable.
streamConcat :: Monad m => Stream (Of [a]) m r -> Stream (Of a) m r
streamConcat = go
  where
    go = lift . Q.next >=> \case
      Left          r -> pure r
      Right (vs0,vss) -> each vs0
        where
          each [    ] = go vss
          each (v:vs) = Q.cons v (each vs)
{-# INLINE streamConcat #-}

instance (Functor f, PrimMonad m) => PrimMonad (Stream f m) where
    type PrimState (Stream f m) = PrimState m
    primitive = lift . primitive

-- | Concatenates the singleton lists in a stream of lists into a plain
-- stream, discards all other lists.
singlesOnly :: Monad m => Stream (Of [a]) m r -> Stream (Of a) m r
singlesOnly = go
  where
    go = lift . Q.next >=> \case
      Left          r -> pure r
      Right ([v],vss) -> Q.cons v (go vss)
      Right ( _ ,vss) -> go vss
{-# INLINE singlesOnly #-}

vconcats :: (V.Vector v a, PrimMonad m) => Stream (Of (v a)) m r -> m (Of (V.Mutable v (PrimState m) a) r)
vconcats s = do v <- M.unsafeNew 1024 ; go v 0 s
  where
    go !v !i = Q.next >=> \case
        Left    r    -> return (M.take i v :> r)
        Right (x,xs) -> do
            let lx = V.length x
            v' <- if i + lx <= M.length v
                    then return v
                    else M.grow v (M.length v `max` lx)
            M.move (M.slice i lx v') =<< V.unsafeThaw x
            go v' (i + lx) xs
{-# INLINABLE vconcats #-}

vfroms :: (M.MVector v a, PrimMonad m) => Stream (Of a) m r -> m (Of (v (PrimState m) a) r)
vfroms s = do v <- M.unsafeNew 1024 ; go v 0 s
  where
    go !v !i = Q.next >=> \case
        Left    r    -> return (M.take i v :> r)
        Right (x,xs) -> do
            v' <- if i < M.length v
                    then return v
                    else M.grow v (M.length v)
            M.write v' i x
            go v' (i+1) xs
{-# INLINABLE vfroms #-}

-- | Errors that should never happen and indicate a bug.
data InternalError = InternalError String deriving Show

-- | Errors that happen because of malformed input.
data DataError = DataError (Maybe FilePath) String deriving Show

-- | Errors that happen because the user did something nonsensical.
data PebkacError = PebkacError String deriving Show

instance Exception InternalError
instance Exception DataError
instance Exception PebkacError

deriving instance Typeable InternalError
deriving instance Typeable DataError
deriving instance Typeable PebkacError
