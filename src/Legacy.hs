module Legacy ( main_legacy ) where

-- ^ Support for legacy files.  Transparent support is awkward, so we
-- only implement a way to upgrade them.  (If you have legacy
-- heffalumps, of all things, this limited support isn't your biggest
-- trouble.)

import Bio.Prelude hiding ( Ns )
import ByteStream                       ( ByteStream, nextByte )
import Genome
import Streaming                        ( Stream, Of(..), MonadIO(..), MonadTrans(..) )
import Streaming.Prelude                ( cons, next, effects )
import System.Console.GetOpt
import System.Directory                 ( doesDirectoryExist, getDirectoryContents, renameFile )
import Util

import qualified Data.ByteString            as B
import qualified ByteStream                 as S
import qualified Data.Vector.Unboxed        as U
import qualified Lump                       as L
import qualified Streaming.Prelude          as Q


-- ^ A genome is encoded by taking the difference to the reference and
-- run-length coding the result.  All stretch lengths are even, so we
-- pre-divide them by two.
--
-- This encoding had a number of unfixable problems, so it is only
-- supported for backwards compatibility:  the encoding in pairs created
-- more problems than it solved, it is impossible to encode indels,
-- haploid and diploid calls cannot be mixed.

data Stretch = Ns   !Int64
             | Eqs  !Int64
             | Eqs1 !Int64
             | Chrs !Word8 !Word8
             | Break

-- We need to store 11 different symbols: "NACGTMRWSYK"  Long
-- matching stretches and long stretches of no call ('N') shall be
-- run-length encoded.  Since 11^2 == 121 < 128, we pack two symbols and
-- a signal bit into one byte.  Also, to avoid messing with nybbles, all
-- counts shall be multiples of two.
--
-- 0xxxxxxxb: two symbols are encoded as x+11*y where x and y are the
--            positions of the symbols in the string above.
-- 10000000b: 'Break'
-- 100xxxxxb: 1-31 Ns
-- 1010xxxxb: together with the next byte, 0-8191 Ns
-- 10110xxxb: together with the next two bytes, 0-1048575 Ns
-- 101110xxb: together with the next three bytes, 0-134217727 Ns
-- 1011110xb: together with the next four bytes, 0-17179869183 Ns
--            (this shouldn't be needed)
-- 110xxxxxb: 0-31 matches
-- 1110xxxxb: together with the next byte, 0-8191 matches
-- 11110xxxb: together with the next two bytes, 0-1048575 matches
--            (this will rarely be needed)
-- 111110xxb: together with the next three bytes, 0-134217727 matches
--            (this shouldn't be needed)
-- 1111110xb: together with the next four bytes, 0-17179869183 matches
--            (this shouldn't be needed)

-- Decoding produces one stretch until it finds the end marker or runs
-- out of input.  We shall get one 'Stretch' for each chromosome.

decode_dip, decode_hap :: Monad m => Maybe FilePath -> ByteStream m r -> Stream (Of Stretch) m r
decode_dip = decode_v0 id Eqs
decode_hap = decode_v0 nc Eqs1
  where
    nc x |    x == 0 = 0
         |    x <= 4 = x+10
         | otherwise = x


-- | Decode for original format, parameterized so it can be used for
-- haploid and diploid individuals (aka "high coverage" and "low
-- coverage" input).
{-# DEPRECATED decode_v0 "Switch to Lumps" #-}
decode_v0 :: Monad m => (Word8 -> Word8) -> (Int64 -> Stretch) -> Maybe FilePath
                     -> ByteStream m r -> Stream (Of Stretch) m r
decode_v0 nucCode eqs fp = go
  where
    go = lift . nextByte >=> \case
       Left    r     -> pure r
       Right (w,s')
          | w < 0x80 -> let (y,x) = w `divMod` 11 in Chrs (nucCode x) (nucCode y) `cons` go s'
          | w ==0x80 -> Break `cons` go s'

          | w < 0xA0 -> Ns (fromIntegral (w .&. 0x1f)) `cons` go s'
          | w < 0xB0 -> do Right (w1,s1) <- lift $ nextByte s'
                           Ns (fromIntegral (w .&. 0x0f) .|.
                               fromIntegral w1 `shiftL` 4) `cons` go s1
          | w < 0xB8 -> do Right (w1,s1) <- lift $ nextByte s'
                           Right (w2,s2) <- lift $ nextByte s1
                           Ns (fromIntegral (w .&. 0x07) .|.
                               fromIntegral w1 `shiftL` 3 .|.
                               fromIntegral w2 `shiftL` 11) `cons` go s2
          | w < 0xBC -> do Right (w1,s1) <- lift $ nextByte s'
                           Right (w2,s2) <- lift $ nextByte s1
                           Right (w3,s3) <- lift $ nextByte s2
                           Ns (fromIntegral (w .&. 0x03) .|.
                               fromIntegral w1 `shiftL` 2 .|.
                               fromIntegral w2 `shiftL` 10 .|.
                               fromIntegral w3 `shiftL` 18) `cons` go s3
          | w < 0xBE -> do Right (w1,s1) <- lift $ nextByte s'
                           Right (w2,s2) <- lift $ nextByte s1
                           Right (w3,s3) <- lift $ nextByte s2
                           Right (w4,s4) <- lift $ nextByte s3
                           Ns (fromIntegral (w .&. 0x01) .|.
                               fromIntegral w1 `shiftL` 1 .|.
                               fromIntegral w2 `shiftL` 9 .|.
                               fromIntegral w3 `shiftL` 17 .|.
                               fromIntegral w4 `shiftL` 25) `cons` go s4
          | w < 0xC0 -> throw . DataError fp $ "unexpected code " ++ show w

          | w < 0xE0 -> eqs (fromIntegral (w .&. 0x1f)) `cons` go s'
          | w < 0xF0 -> do Right (w1,s1) <- lift $ nextByte s'
                           eqs (fromIntegral (w .&. 0x0f) .|.
                                fromIntegral w1 `shiftL` 4) `cons` go s1
          | w < 0xF8 -> do Right (w1,s1) <- lift $ nextByte s'
                           Right (w2,s2) <- lift $ nextByte s1
                           eqs (fromIntegral (w .&. 0x07) .|.
                                fromIntegral w1 `shiftL` 3 .|.
                                fromIntegral w2 `shiftL` 11) `cons` go s2
          | otherwise -> throw . DataError fp $ "unexpected code " ++ show w

data Config = Config
    { conf_reference :: FilePath
    , conf_recursive :: Bool
    , conf_keep      :: Bool
    , conf_force     :: Bool
    , conf_output    :: Maybe FilePath }

config :: Config
config = Config (throw $ PebkacError "no reference given") False False False Nothing

options :: [ OptDescr (Config -> IO Config) ]
options =
    [ Option "k" ["keep"]      (NoArg  set_keep        ) "Keep (don't delete) input files"
    , Option "f" ["force"]     (NoArg  set_force       ) "Convert even if it makes no sense"
    , Option "r" ["reference"] (ReqArg set_ref   "FILE") "Read reference from FILE (.2bit)"
    , Option "R" ["recursive"] (NoArg  set_rec         ) "Operate recursively on directories"
    , Option "o" ["output"]    (ReqArg set_ofile "FILE") "Write output to FILE" ]
  where
    set_keep    c = return $ c { conf_keep      =   True }
    set_force   c = return $ c { conf_force     =   True }
    set_rec     c = return $ c { conf_recursive =   True }
    set_ofile a c = return $ c { conf_output    = Just a }
    set_ref   a c = return $ c { conf_reference =      a }


-- | Converts old style 'Stretch' to new style 'Lump'.  Unfortunately,
-- this needs a reference.
stretchToLump :: Monad m => RefSeqs -> Stream (Of Stretch) m r -> Stream (Of L.Lump) m r
stretchToLump nrs = L.normalizeLump . go1 (rss_seqs nrs)
  where
    go1 [     ] l = lift $ effects l
    go1 (r0:rs) l = go (r0 ()) l
      where
        go r = lift . next >=> \case
            Right (Chrs a b,k) -> call a (call b (`go` k)) r
            Right (Ns     c,k) -> L.Ns   (fromIntegral $ c+c) `cons` go (dropRS (fromIntegral $ c+c) r) k
            Right (Eqs    c,k) -> L.Eqs2 (fromIntegral $ c+c) `cons` go (dropRS (fromIntegral $ c+c) r) k
            Right (Eqs1   c,k) -> L.Eqs1 (fromIntegral $ c+c) `cons` go (dropRS (fromIntegral $ c+c) r) k
            Right (Break   ,k) -> L.Break                     `cons` go1 rs k
            Left z             -> pure z

    call :: Monad m => Word8 -> (RefSeq -> Stream (Of L.Lump) m r) -> RefSeq -> Stream (Of L.Lump) m r
    call b k r = case unconsRS r of
        Nothing     -> lift $ Q.effects (k r)
        Just (a,r') -> L.encTwoNucs a (nc2vs U.! fromIntegral b) `Q.cons` k r'

    nc2vs :: U.Vector Word8
    !nc2vs = U.fromList [ 255,  5, 10, 15, 0,        -- NACGT
                            6,  7,  1, 11, 2, 3,     -- MRWSYK
                           17, 18, 19, 16, 255 ]     -- acgtN



data DecodeResult = Legacy  | Modern | Unknown

-- | Legacy decoder.  Switches behavior based on header.  "HEF\0",
-- "HEF\1" are the old format and go through 'Stretch' to 'Lump'
-- conversion;  For the old format, we need a reference, and we have to
-- trust it's a compatible one.  "HEF\3" is the new format.  We could
-- check if the reference is compatible, but chose not to.  This is only
-- used if explicitly requested, and then we might as well trust the
-- user.  ("HEF\2" was an experiment that never left the lab.)
--
-- We try to support ancient files without format marker.  These could
-- start with anything, but in practice start with either (Ns 5000) or
-- (Ns 5001).  Anything else is treated as unknown junk.
decode_legacy :: MonadIO m => RefSeqs -> Maybe FilePath -> ByteStream m r -> m (DecodeResult, Stream (Of L.Lump) m r)
decode_legacy rs fp str = do
        hd :> tl <- S.toStrict $ S.splitAt 4 str
        case B.take 3 hd :> B.drop 3 hd of
            "HEF" :> "\0"     -> return $ (,) Legacy  $ stretchToLump rs $ decode_dip fp tl
            "HEF" :> "\1"     -> return $ (,) Legacy  $ stretchToLump rs $ decode_hap fp tl
            "HEF" :> "\3"     -> return $ (,) Modern  $ L.decodeLump  fp $ S.drop 5 $ S.dropWhile (/= 0) tl
            "\176\113\2" :> _ -> return $ (,) Legacy  $ stretchToLump rs $ decode_dip fp $ S.chunk hd >> tl
            "\177\113\2" :> _ -> return $ (,) Legacy  $ stretchToLump rs $ decode_dip fp $ S.chunk hd >> tl
            _            :> _ -> return $ (,) Unknown $ stretchToLump rs $ decode_dip fp $ S.chunk hd >> tl


scan_fs :: [FilePath] -> IO [FilePath]
scan_fs [] = return []
scan_fs (x:xs) = do d <- doesDirectoryExist x
                    if d then do ys <- getDirectoryContents x
                                 scan_fs $ [ x ++ "/" ++ y | y <- ys ] ++ xs
                         else (:) x `liftM` scan_fs xs


main_legacy :: [String] -> IO ()
main_legacy args = do
    ( hefs0, Config{..} ) <- parseFileOpts config "legacy" "[hef-file...]" options args

    reference <- readTwoBit conf_reference
    hefs <- if conf_recursive then scan_fs hefs0 else return hefs0

    let write fp s = S.writeFile fp $ S.gzip $ L.encode reference s
        conv k fp = withInputFile fp $ \ihdl -> do
                        decode_legacy reference (Just fp) (S.decomp (S.hGetContents ihdl)) >>= \case
                            (Legacy,  s)        -> do hPutStrLn stderr $ fp ++ ": converting from legacy format" ; k s
                            (_, s) | conf_force -> do hPutStrLn stderr $ fp ++ ": converting because of --force" ; k s
                            (Unknown, _)        ->    hPutStrLn stderr $ fp ++ ": looks like junk, use --force to convert anyway"
                            (Modern,  _)        ->    hPutStrLn stderr $ fp ++ ": looks like a modern file, no conversion needed"
    case conf_output of
        Just ofp ->
            case hefs of
                [   ] -> return ()
                [ifp] -> conv (write ofp) ifp
                _     -> throwIO $ PebkacError "--output requires exactly one input file"

        Nothing  -> forM_ hefs $ \fp -> conv (go fp) fp
          where
            go fp s = do when conf_keep $ renameFile fp (fp ++ "~")
                         write fp s
