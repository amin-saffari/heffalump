{-# LANGUAGE CPP, RankNTypes #-}

-- This library emulates Data.ByteStream.Lazy but includes a monadic element
-- and thus at certain points uses a `Stream`/`FreeT` type in place of lists.

-- |
-- Module      : ByteStream
-- Copyright   : (c) Don Stewart 2006
--               (c) Duncan Coutts 2006-2011
--               (c) Michael Thompson 2015
--               (c) Udo Stenzel 2018
-- License     : BSD-style
--
-- Maintainer  : u.stenzel@web.de
-- Stability   : experimental
-- Portability : portable
--
-- See the simple examples of use <https://gist.github.com/michaelt/6c6843e6dd8030e95d58 here>.
-- We begin with a slight modification of the documentation to "Data.ByteStream.Lazy":
--
-- A time and space-efficient implementation of effectful byte streams
-- using a stream of packed 'Word8' arrays, suitable for high performance
-- use, both in terms of large data quantities, or high speed
-- requirements. ByteStreams are encoded as streams of strict chunks
-- of bytes.
--
-- A key feature of ByteStreams is the means to manipulate large or
-- unbounded streams of data without requiring the entire sequence to be
-- resident in memory. To take advantage of this you have to write your
-- functions in a streaming style, e.g. classic pipeline composition. The
-- default I\/O chunk size is 32k, which should be good in most circumstances.
--
-- Some operations, such as 'concat', 'append', 'reverse' and 'cons', have
-- better complexity than their "Data.ByteStream" equivalents, due to
-- optimisations resulting from the list spine structure. For other
-- operations streaming, like lazy, ByteStreams are usually within a few percent of
-- strict ones.
--
-- This module is intended to be imported @qualified@, to avoid name
-- clashes with "Prelude" functions.  eg.
--
-- > import qualified ByteStream as S
--
-- Original GHC implementation by Bryan O\'Sullivan.
-- Rewritten to use 'Data.Array.Unboxed.UArray' by Simon Marlow.
-- Rewritten to support slices and use 'Foreign.ForeignPtr.ForeignPtr'
-- by David Roundy.
-- Rewritten again and extended by Don Stewart and Duncan Coutts.
-- Lazy variant by Duncan Coutts and Don Stewart.
-- Streaming variant by Michael Thompson, following the ideas of Gabriel Gonzales' pipes-bytestring
-- Cut down and renamed for internal use by heffalump by Udo Stenzel.
--
module ByteStream (
    -- * The @ByteStream@ type
    ByteStream

    -- * Introducing and eliminating 'ByteStream's
    , empty            -- empty :: ByteStream m ()
    , singleton        -- singleton :: Monad m => Word8 -> ByteStream m ()
    , fromLazy         -- fromLazy :: Monad m => ByteStream -> ByteStream m ()
    , toLazy           -- toLazy :: Monad m => ByteStream m () -> m ByteStream
    , toStrict         -- toStrict :: Monad m => ByteStream m () -> m ByteStream
    , effects
    , mwrap

    -- * Basic interface
    , cons             -- cons :: Monad m => Word8 -> ByteStream m r -> ByteStream m r
    , nextByte         -- nextByte :: Monad m => ByteStream m r -> m (Either r (Word8, ByteStream m r))

    -- * Substrings

    -- ** Breaking strings
    , break            -- break :: Monad m => (Word8 -> Bool) -> ByteStream m r -> ByteStream m (ByteStream m r)
    , drop             -- drop :: Monad m => GHC.Int.Int64 -> ByteStream m r -> ByteStream m r
    , dropWhile
    , splitAt          -- splitAt :: Monad m => GHC.Int.Int64 -> ByteStream m r -> ByteStream m (ByteStream m r)

    -- ** Breaking into many substrings
    , lines
    , lines'

    -- ** Special folds
    , concat          -- concat :: Monad m => Stream (ByteStream m) m r -> ByteStream m r

    -- * Builders
    , toByteStream
    , toByteStreamWith
    , concatBuilders

    -- * I\/O with 'ByteStream's

    -- ** Files
    , withOutputFile
    , writeFile        -- writeFile :: FilePath -> ByteStream IO r -> IO r

    -- ** I\/O with Handles
    , hGetContents     -- hGetContents :: Handle -> ByteStream IO ()
    , hGetContentsN    -- hGetContentsN :: Int -> Handle -> ByteStream IO ()
    , hPut             -- hPut :: Handle -> ByteStream IO r -> IO r

    -- * Simple chunkwise operations
    , nextChunk
    , consChunk             -- :: S.ByteString -> ByteStream m r -> ByteStream m r
    , chunk
    , copy

    -- * gzip and bgzip support
    , decomp
    , decompWith
    , gzip
    , gunzip
  ) where

import Bio.Prelude                      hiding (break,concat,drop,dropWhile,lines,splitAt,writeFile,empty,loop)
import Data.ByteString.Builder.Internal
        (Builder,builder,runBuilder,runBuilderWith,bufferSize
        ,AllocationStrategy,ChunkIOStream(..),buildStepToCIOS
        ,byteStringFromBuffer,safeStrategy,defaultChunkSize)
import GHC.Exts                                (SpecConstrAnnotation(..))
import Streaming                               (Of(..),Identity(..),MonadIO(..),MonadTrans(..),destroy)
import Streaming.Internal                      (Stream (..))
import System.Directory                        (renameFile)
import System.IO                               (withBinaryFile,IOMode(..))

import qualified Codec.Compression.Zlib.Internal as Z
import qualified Data.ByteString                 as S  -- S for strict (hmm...)
import qualified Data.ByteString.Internal        as S
import qualified Data.ByteString.Lazy.Internal   as BI  -- just for fromChunks etc
import qualified Data.ByteString.Unsafe          as S
import qualified Streaming.Prelude               as Q

-- | A space-efficient representation of a succession of 'Word8' vectors, supporting many
-- efficient operations.
--
-- An effectful 'ByteStream' contains 8-bit bytes, or by using the operations
-- from "Data.ByteStream.Streaming.Char8" it can be interpreted as containing
-- 8-bit characters.

data ByteStream m r =
  Empty r
  | Chunk {-#UNPACK #-} !S.ByteString (ByteStream m r )
  | Go (m (ByteStream m r ))

instance Monad m => Functor (ByteStream m) where
  fmap f x = case x of
    Empty a      -> Empty (f a)
    Chunk bs bss -> Chunk bs (fmap f bss)
    Go mbss      -> Go (liftM (fmap f) mbss)

instance Monad m => Applicative (ByteStream m) where
  pure = Empty
  {-#INLINE pure #-}
  bf <*> bx = do {f <- bf; x <- bx; Empty (f x)}
  {-#INLINE (<*>) #-}
  (*>) = (>>)
  {-#INLINE (*>) #-}

instance Monad m => Monad (ByteStream m) where
  return = Empty
  {-#INLINE return #-}
  x0 >> y = loop SPEC x0 where
    loop !_ x = case x of   -- this seems to be insanely effective
      Empty _ -> y
      Chunk a b -> Chunk a (loop SPEC b)
      Go m -> Go (liftM (loop SPEC) m)
  {-#INLINEABLE (>>)#-}
  x >>= f =
    loop SPEC2 x where -- unlike >> this SPEC seems pointless
      loop !_ y = case y of
        Empty a -> f a
        Chunk bs bss -> Chunk bs (loop SPEC bss)
        Go mbss      -> Go (liftM (loop SPEC) mbss)
  {-#INLINEABLE (>>=) #-}

instance MonadIO m => MonadIO (ByteStream m) where
  liftIO io = Go (liftM Empty (liftIO io))
  {-#INLINE liftIO #-}

instance MonadTrans ByteStream where
  lift ma = Go $ liftM Empty ma
  {-#INLINE lift #-}

instance (r ~ ()) => IsString (ByteStream m r) where
  fromString = chunk . fromString
  {-#INLINE fromString #-}

instance (m ~ Identity, Show r) => Show (ByteStream m r) where
  show bs0 = case bs0 of  -- the implementation this instance deserves ...
    Empty r           -> "Empty (" ++ show r ++ ")"
    Go (Identity bs') -> "Go (Identity (" ++ show bs' ++ "))"
    Chunk bs'' bs     -> "Chunk " ++ show bs'' ++ " (" ++ show bs ++ ")"

#if MIN_VERSION_base(4,11,0)
instance (Semigroup r, Monad m) => Semigroup (ByteStream m r) where
  (<>) = liftM2 (<>)
  {-# INLINE (<>) #-}
#endif

instance (Monoid r, Monad m) => Monoid (ByteStream m r) where
  mempty = Empty mempty
  {-# INLINE mempty #-}
  mappend = liftM2 mappend
  {-# INLINE mappend #-}


data SPEC = SPEC | SPEC2
{-# ANN type SPEC ForceSpecConstr #-}

-- -- ------------------------------------------------------------------------
--
-- | Smart constructor for 'Chunk'.
consChunk :: S.ByteString -> ByteStream m r -> ByteStream m r
consChunk c@(S.PS _ _ len) cs
  | len == 0  = cs
  | otherwise = Chunk c cs
{-# INLINE consChunk #-}

-- | Yield-style smart constructor for 'Chunk'.
chunk :: S.ByteString -> ByteStream m ()
chunk bs = consChunk bs (Empty ())
{-# INLINE chunk #-}


{- | Reconceive an effect that results in an effectful bytestring as an effectful bytestring.
    Compare Streaming.mwrap. The closes equivalent of

>>> Streaming.wrap :: f (Stream f m r) -> Stream f m r

    is here  @consChunk@. @mwrap@ is the smart constructor for the internal @Go@ constructor.
-}
mwrap :: m (ByteStream m r) -> ByteStream m r
mwrap = Go
{-# INLINE mwrap #-}

-- | Construct a succession of chunks from its Church encoding (compare @GHC.Exts.build@)
materialize :: (forall x . (r -> x) -> (S.ByteString -> x -> x) -> (m x -> x) -> x)
            -> ByteStream m r
materialize phi = phi Empty Chunk Go
{-#INLINE[0] materialize #-}

-- | Resolve a succession of chunks into its Church encoding; this is
-- not a safe operation; it is equivalent to exposing the constructors
dematerialize :: Monad m
              => ByteStream m r
              -> (forall x . (r -> x) -> (S.ByteString -> x -> x) -> (m x -> x) -> x)
dematerialize x0 nil con fin = loop SPEC x0
  where
  loop !_ x = case x of
     Empty r    -> nil r
     Chunk b bs -> con b (loop SPEC bs )
     Go ms -> fin (liftM (loop SPEC) ms)
{-# INLINE [1] dematerialize #-}

{-# RULES
  "dematerialize/materialize" forall (phi :: forall b . (r -> b) -> (S.ByteString -> b -> b) -> (m b -> b)  -> b). dematerialize (materialize phi) = phi ;
  #-}
------------------------------------------------------------------------

-- The representation uses lists of packed chunks. When we have to convert from
-- a lazy list to the chunked representation, then by default we use this
-- chunk size. Some functions give you more control over the chunk size.
--
-- Measurements here:
--  http://www.cse.unsw.edu.au/~dons/tmp/chunksize_v_cache.png
--
-- indicate that a value around 0.5 to 1 x your L2 cache is best.
-- The following value assumes people have something greater than 128k,
-- and need to share the cache with other programs.

copy :: Monad m => ByteStream m r -> ByteStream (ByteStream m) r
copy = loop where
  loop str = case str of
    Empty r         -> Empty r
    Go m            -> Go (liftM loop (lift m))
    Chunk bs rest   -> Chunk bs (Go (Chunk bs (Empty (loop rest))))
{-# INLINABLE copy #-}

-- | /O(n)/ Concatenate a stream of byte streams.
concat :: Monad m => Stream (ByteStream m) m r -> ByteStream m r
concat x = destroy x join Go Empty
{-# INLINE concat #-}

-- | Perform the effects contained in an effectful bytestring, ignoring the bytes.
effects :: Monad m => ByteStream m r -> m r
effects bs = case bs of
  Empty r      -> return r
  Go m         -> m >>= effects
  Chunk _ rest -> effects rest
{-# INLINABLE effects #-}


-- -----------------------------------------------------------------------------
-- Introducing and eliminating 'ByteStream's

{-| /O(1)/ The empty 'ByteStream' -- i.e. @return ()@ Note that @ByteStream m w@ is
  generally a monoid for monoidal values of @w@, like @()@
-}
empty :: ByteStream m ()
empty = Empty ()
{-# INLINE empty #-}

{-| /O(1)/ Yield a 'Word8' as a minimal 'ByteStream'
-}
singleton :: Monad m => Word8 -> ByteStream m ()
singleton w = Chunk (S.singleton w)  (Empty ())
{-# INLINE singleton #-}

{-| /O(c)/ Convert a byte stream into a stream of individual strict bytestrings.
    This of course exposes the internal chunk structure.
-}
toChunks :: Monad m => ByteStream m r -> Stream (Of S.ByteString) m r
toChunks bs =
  dematerialize bs
      return
      (\b mx -> Step (b:> mx))
      Effect
{-# INLINE toChunks #-}

{-| /O(n)/ Convert a monadic byte stream into a single strict 'ByteStream',
   retaining the return value of the original pair. This operation is
   for use with 'mapped'.

> mapped R.toStrict :: Monad m => Stream (ByteStream m) m r -> Stream (Of ByteStream) m r

   It is subject to all the objections one makes to Data.ByteStream.Lazy 'toStrict';
   all of these are devastating.
-}
toStrict :: Monad m => ByteStream m r -> m (Of S.ByteString r)
toStrict bs = do
  (bss :> r) <- Q.toList (toChunks bs)
  return $ (S.concat bss :> r)
{-# INLINE toStrict #-}

{- |/O(c)/ Transmute a pseudo-pure lazy bytestring to its representation
    as a monadic stream of chunks.

>>> Q.putStrLn $ Q.fromLazy "hi"
hi
>>>  Q.fromLazy "hi"
Chunk "hi" (Empty (()))  -- note: a 'show' instance works in the identity monad
>>>  Q.fromLazy $ BL.fromChunks ["here", "are", "some", "chunks"]
Chunk "here" (Chunk "are" (Chunk "some" (Chunk "chunks" (Empty (())))))

-}
fromLazy :: Monad m => BI.ByteString -> ByteStream m ()
fromLazy = BI.foldrChunks Chunk (Empty ())
{-# INLINE fromLazy #-}

{-| /O(n)/ Convert an effectful byte stream into a single lazy 'ByteStream'
    with the same internal chunk structure, retaining the original
    return value.

    This is the canonical way of breaking streaming (@toStrict@ and the
    like are far more demonic). Essentially one is dividing the interleaved
    layers of effects and bytes into one immense layer of effects,
    followed by the memory of the succession of bytes.

    Because one preserves the return value, @toLazy@ is a suitable argument
    for 'Streaming.mapped'

>   S.mapped Q.toLazy :: Stream (ByteStream m) m r -> Stream (Of L.ByteStream) m r

>>> Q.toLazy "hello"
"hello" :> ()
>>> S.toListM $ traverses Q.toLazy $ Q.lines "one\ntwo\nthree\nfour\nfive\n"
["one","two","three","four","five",""]  -- [L.ByteStream]

-}
toLazy :: Monad m => ByteStream m r -> m (Of BI.ByteString r)
toLazy bs0 = dematerialize bs0
                (\r -> return (BI.Empty :> r))
                (\b mx -> do
                      (bs :> x) <- mx
                      return $ BI.Chunk b bs :> x
                      )
                join
{-#INLINE toLazy #-}

-- | /O(1)/ 'cons' is analogous to '(:)' for lists.
cons :: Monad m => Word8 -> ByteStream m r -> ByteStream m r
cons c cs = Chunk (S.singleton c) cs
{-# INLINE cons #-}

-- | /O(1)/ Extract the head and tail of a 'ByteStream', or its return value
-- if it is empty. This is the \'natural\' uncons for an effectful byte stream.
nextByte :: Monad m => ByteStream m r -> m (Either r (Word8, ByteStream m r))
nextByte (Empty r) = return (Left r)
nextByte (Chunk c cs)
    = if S.null c
        then nextByte cs
        else return $ Right (S.unsafeHead c
                     , if S.length c == 1
                         then cs
                         else Chunk (S.unsafeTail c) cs )
nextByte (Go m) = m >>= nextByte
{-# INLINABLE nextByte #-}

nextChunk :: Monad m => ByteStream m r -> m (Either r (S.ByteString, ByteStream m r))
nextChunk = \bs -> case bs of
  Empty r    -> return (Left r)
  Chunk c cs -> if S.null c
    then nextChunk cs
    else return (Right (c,cs))
  Go m       -> m >>= nextChunk
{-# INLINABLE nextChunk #-}

{-| /O(n\/c)/ 'drop' @n xs@ returns the suffix of @xs@ after the first @n@
    elements, or @[]@ if @n > 'length' xs@.

>>> Q.putStrLn $ Q.drop 6 "Wisconsin"
sin
>>> Q.putStrLn $ Q.drop 16 "Wisconsin"

>>>
-}
drop  :: Monad m => Int64 -> ByteStream m r -> ByteStream m r
drop i p | i <= 0 = p
drop i cs0 = drop' i cs0
  where drop' 0 cs           = cs
        drop' _ (Empty r)    = Empty r
        drop' n (Chunk c cs) =
          if n < fromIntegral (S.length c)
            then Chunk (S.drop (fromIntegral n) c) cs
            else drop' (n - fromIntegral (S.length c)) cs
        drop' n (Go m) = Go (liftM (drop' n) m)
{-# INLINABLE drop #-}


{-| /O(n\/c)/ 'splitAt' @n xs@ is equivalent to @('take' n xs, 'drop' n xs)@.

>>> rest <- Q.putStrLn $ Q.splitAt 3 "therapist is a danger to good hyphenation, as Knuth notes"
the
>>> Q.putStrLn $ Q.splitAt 19 rest
rapist is a danger

-}
splitAt :: Monad m => Int64 -> ByteStream m r -> ByteStream m (ByteStream m r)
splitAt i cs0 | i <= 0 = Empty cs0
splitAt i cs0 = splitAt' i cs0
  where splitAt' 0 cs           = Empty cs
        splitAt' _ (Empty r  )   = Empty (Empty r)
        splitAt' n (Chunk c cs) =
          if n < fromIntegral (S.length c)
            then Chunk (S.take (fromIntegral n) c) $
                     Empty (Chunk (S.drop (fromIntegral n) c) cs)
            else Chunk c (splitAt' (n - fromIntegral (S.length c)) cs)
        splitAt' n (Go m) = Go  (liftM (splitAt' n) m)
{-# INLINABLE splitAt #-}

-- | 'dropWhile' @p xs@ returns the suffix remaining after 'takeWhile' @p xs@.
dropWhile :: Monad m => (Word8 -> Bool) -> ByteStream m r -> ByteStream m r
dropWhile pr = drop' where
  drop' bs = case bs of
    Empty r    -> Empty r
    Go m       -> Go (liftM drop' m)
    Chunk c cs -> case findIndexOrEnd (not.pr) c of
        0                  -> Chunk c cs
        n | n < S.length c -> Chunk (S.drop n c) cs
          | otherwise      -> drop' cs
{-#INLINABLE dropWhile #-}

-- | 'break' @p@ is equivalent to @'span' ('not' . p)@.
break :: Monad m => (Word8 -> Bool) -> ByteStream m r -> ByteStream m (ByteStream m r)
break f cs0 = break' cs0
  where break' (Empty r)        = Empty (Empty r)
        break' (Chunk c cs) =
          case findIndexOrEnd f c of
            0                  -> Empty (Chunk c cs)
            n | n < S.length c -> Chunk (S.take n c) $
                                      Empty (Chunk (S.drop n c) cs)
              | otherwise      -> Chunk c (break' cs)
        break' (Go m) = Go (liftM break' m)
{-# INLINABLE break #-}

{- | Read entire handle contents /lazily/ into a 'ByteStream'. Chunks
    are read on demand, in at most @k@-sized chunks. It does not block
    waiting for a whole @k@-sized chunk, so if less than @k@ bytes are
    available then they will be returned immediately as a smaller chunk.

    The handle is closed on EOF.

    Note: the 'Handle' should be placed in binary mode with
    'System.IO.hSetBinaryMode' for 'hGetContentsN' to
    work correctly.
-}
hGetContentsN :: MonadIO m => Int -> Handle -> ByteStream m ()
hGetContentsN k h = loop
  where
    loop = do
        c <- liftIO (S.hGetSome h k)
        -- only blocks if there is no data available
        if S.null c
          then Empty ()
          else Chunk c loop
{-#INLINABLE hGetContentsN #-} -- very effective inline pragma

{-| Read entire handle contents /lazily/ into a 'ByteStream'. Chunks
    are read on demand, using the default chunk size.

    Note: the 'Handle' should be placed in binary mode with
    'System.IO.hSetBinaryMode' for 'hGetContents' to
    work correctly.
-}
hGetContents :: MonadIO m => Handle -> ByteStream m ()
hGetContents = hGetContentsN defaultChunkSize
{-#INLINE hGetContents #-}


withOutputFile :: FilePath -> (Handle -> IO a) -> IO a
withOutputFile "-" k = k stdout
withOutputFile  f  k = withBinaryFile (f++".#~#") WriteMode $ \hdl ->
                       k hdl >>= \r -> renameFile (f++".#~#") f >> return r

{-| Writes a 'ByteStream' to a file.  Uses 'bracket'
    to ensure that the handle is closed.  Actually write to a temporary
    file and renames it on successful completion.
 -}
writeFile :: FilePath -> ByteStream IO r -> IO r
writeFile f str = withOutputFile f $ \hdl -> hPut hdl str
{-# INLINE writeFile #-}

-- | Outputs a 'ByteStream' to the specified 'Handle'.
--
hPut ::  MonadIO m => Handle -> ByteStream m r -> m r
hPut h cs = dematerialize cs return (\x y -> liftIO (S.hPut h x) >> y) (>>= id)
{-#INLINE hPut #-}

-- -- ---------------------------------------------------------------------
-- -- Internal utilities

-- | 'findIndexOrEnd' is a variant of findIndex, that returns the length
-- of the string if no element is found, rather than Nothing.
findIndexOrEnd :: (Word8 -> Bool) -> S.ByteString -> Int
findIndexOrEnd k (S.PS x s l) =
#if MIN_VERSION_bytestring(0,10,8)
    S.accursedUnutterablePerformIO $
#else
    S.inlinePerformIO $
#endif
        withForeignPtr x $ \f -> go (f `plusPtr` s) 0
  where
    go !ptr !n | n >= l    = return l
               | otherwise = do w <- peek ptr
                                if k w
                                  then return n
                                  else go (ptr `plusPtr` 1) (n+1)
{-# INLINABLE findIndexOrEnd #-}

{- Take a builder constructed otherwise and convert it to a genuine
   streaming bytestring.

>>>  Q.putStrLn $ Q.toByteStream $ stringUtf8 "哈斯克尔" <> stringUtf8 " " <> integerDec 98
哈斯克尔 98

    <https://gist.github.com/michaelt/6ea89ca95a77b0ef91f3 This benchmark> shows its
    indistinguishable performance is indistinguishable from @toLazyByteStream@
-}

toByteStream
  :: MonadIO m => Builder -> ByteStream m ()
toByteStream = toByteStreamWith (safeStrategy BI.smallChunkSize BI.defaultChunkSize)
{-#INLINE toByteStream #-}

{-| Take a builder and convert it to a genuine
   streaming bytestring, using a specific allocation strategy.
-}
toByteStreamWith
   :: MonadIO m =>
      AllocationStrategy -> Builder -> ByteStream m ()
toByteStreamWith strategy builder0 = do
       cios <- liftIO (buildStepToCIOS strategy (runBuilder builder0))
       let loop cios0 = case cios0 of
              Yield1 bs io   -> Chunk bs $ do
                    cios1 <- liftIO io
                    loop cios1
              Finished buf r -> trimmedChunkFromBuffer buf (Empty r)
           trimmedChunkFromBuffer buffer k
              | S.null bs                            = k
              |  2 * S.length bs < bufferSize buffer = Chunk (S.copy bs) k
              | otherwise                            = Chunk bs          k
              where
                bs = byteStringFromBuffer buffer
       loop cios
{-#INLINABLE toByteStreamWith #-}
{-#SPECIALIZE toByteStreamWith ::  AllocationStrategy -> Builder -> ByteStream IO () #-}


{- Concatenate a stream of builders (not a streaming bytestring!) into a single builder.

>>> let aa = yield (integerDec 10000) >> yield (string8 " is a number.") >> yield (char8 '\n')
>>>  hPutBuilder  IO.stdout $ concatBuilders aa
10000 is a number.

-}
concatBuilders :: Stream (Of Builder) IO () -> Builder
concatBuilders p = builder $ \bstep r -> do
  case p of
    Return _          -> runBuilderWith mempty bstep r
    Step (b :> rest)  -> runBuilderWith (b `mappend` concatBuilders rest) bstep r
    Effect m            -> m >>= \p' -> runBuilderWith (concatBuilders p') bstep r
{-#INLINABLE concatBuilders #-}

{- | 'lines' turns a ByteStream into a connected stream of ByteStreams at
     divide at newline characters. The resulting strings do not contain newlines.
     This is the genuinely streaming 'lines' which only breaks chunks, and
     thus never increases the use of memory.

     Because 'ByteStream's are usually read in binary mode, with no line
     ending conversion, this function recognizes both @\\n@ and @\\r\\n@
     endings (regardless of the current platform).
-}

lines :: Monad m => ByteStream m r -> Stream (ByteStream m) m r
lines text0 = loop1 text0
  where
    loop1 :: Monad m => ByteStream m r -> Stream (ByteStream m) m r
    loop1 text =
      case text of
        Empty r -> Return r
        Go m -> Effect $ liftM loop1 m
        Chunk c cs
          | S.null c -> loop1 cs
          | otherwise -> Step (loop2 False text)
    loop2 :: Monad m => Bool -> ByteStream m r -> ByteStream m (Stream (ByteStream m) m r)
    loop2 prevCr text =
      case text of
        Empty r -> if prevCr
          then Chunk (S.singleton 13) (Empty (Return r))
          else Empty (Return r)
        Go m -> Go $ liftM (loop2 prevCr) m
        Chunk c cs ->
          case S.elemIndex 10 c of
            Nothing -> if S.null c
              then loop2 prevCr cs
              else if S.unsafeLast c == 13
                then Chunk (S.unsafeInit c) (loop2 True cs)
                else Chunk c (loop2 False cs)
            Just i -> do
              let prefixLength =
                    if i >= 1 && S.unsafeIndex c (i-1) == 13 -- \r\n (dos)
                      then i-1
                      else i
                  rest =
                    if S.length c > i+1
                      then Chunk (S.drop (i+1) c) cs
                      else cs
                  result = Chunk (S.unsafeTake prefixLength c) (Empty (loop1 rest))
              if i > 0 && prevCr
                then Chunk (S.singleton 13) result
                else result
{-#INLINABLE lines #-}

lines' :: Monad m => ByteStream m r -> Stream (Of S.ByteString) m r
lines' = loop1 []
  where
    loop1 :: Monad m => [S.ByteString] -> ByteStream m r -> Stream (Of S.ByteString) m r
    loop1 acc text =
      case text of
        Empty r -> Return r
        Go m -> Effect $ liftM (loop1 acc) m
        Chunk c cs
          | S.null c -> loop1 acc cs
          | otherwise ->
              case S.elemIndex 10 c of
                Just  i -> Q.cons (if null acc then S.take i c else S.concat (reverse (S.take i c : acc)))
                                  (loop1 [] (Chunk (S.drop (i+1) c) cs))
                Nothing -> loop1 (c:acc) cs
{-#INLINABLE lines' #-}


-- | Checks if the input is GZip at all, returns it unchanged if it
-- isn't.  Else runs gunzip.
decomp :: MonadIO m => ByteStream m r -> ByteStream m r
decomp = decompWith return
{-# INLINABLE decomp #-}

-- | Checks if the input is GZip at all, and runs gunzip if it is.  If
-- it isn't, it runs 'k' on the input.
decompWith :: MonadIO m
           => (ByteStream m r -> m (ByteStream m r))
           -> ByteStream m r -> ByteStream m r
decompWith k s0 = mwrap $ nextByte s0 >>= \case
    Right (31, s') -> nextByte s' >>= \case
        Right (139,s'') -> return $ gunzip (cons 31 (cons 139 s''))
        Right ( c, s'') -> return $ cons 31 (cons c s'')
        Left     r      -> return $ cons 31 (pure r)
    Right ( c, s') -> k $ cons c s'
    Left     r     -> k $ pure r
{-# INLINABLE decompWith #-}

-- | Decompresses a gzip stream.  If the leftovers look like another
-- gzip stream, it recurses (some files look like gzip and contain
-- bgzip).  Otherwise, the leftover are discarded (some HETFA files
-- appear to have junk at the end).
gunzip :: MonadIO m => ByteStream m r -> ByteStream m r
gunzip = go $ Z.decompressIO Z.gzipOrZlibFormat Z.defaultDecompressParams
  where
    -- get next chunk, make sure it is empty iff the input ended
    go (Z.DecompressInputRequired next) inp =
        lift (nextChunk inp) >>= \case
            Left r          -> liftIO (next S.empty) >>= flip go (pure r)
            Right (ck,inp')
                | S.null ck -> go (Z.DecompressInputRequired next) inp'
                | otherwise -> liftIO (next ck) >>= flip go inp'

    go (Z.DecompressOutputAvailable outchunk next) inp =
        chunk outchunk >> liftIO next >>= flip go inp

    go (Z.DecompressStreamEnd inchunk) inp =
        -- decompress leftovers if possible, else discard
        decompWith (liftM pure . effects) (chunk inchunk >> inp)

    go (Z.DecompressStreamError derr) _inp =
        liftIO (throwIO derr)

gzip :: MonadIO m => ByteStream m r -> ByteStream m r
gzip = go $ Z.compressIO Z.gzipFormat Z.defaultCompressParams
  where
    -- get next chunk, make sure it is empty iff the input ended
    go (Z.CompressInputRequired next) inp =
        lift (nextChunk inp) >>= \case
            Left r          -> liftIO (next S.empty) >>= flip go (pure r)
            Right (ck,inp')
                | S.null ck -> go (Z.CompressInputRequired next) inp'
                | otherwise -> liftIO (next ck) >>= flip go inp'

    go (Z.CompressOutputAvailable outchunk next) inp =
        chunk outchunk >> liftIO next >>= flip go inp

    go Z.CompressStreamEnd inp = lift (effects inp)


