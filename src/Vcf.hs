{-# LANGUAGE RankNTypes #-}
module Vcf ( main_xcf, conf_vcf, toDensity, RawVariant(..), ConfXcf(..) ) where

-- ^ Stuff related to Vcf and Bcf

import Bio.Prelude               hiding ( Ns )
import Streaming                        ( Stream, Of(..), MonadIO(..), MonadTrans(..), mapsM )
import System.Console.GetOpt
import System.IO

import qualified ByteStream                      as S
import qualified Data.ByteString.Char8           as B
import qualified Data.HashMap.Strict             as M
import qualified Data.IntMap.Strict              as I
import qualified Data.Vector.Unboxed             as U
import qualified Streaming.Prelude               as Q

import Bed
import Genome
import Lump
import Util

-- | Supports one individual only.  (Project your VCF if you have more
-- than one; better yet, use BCF.)
data RawVariant = RawVariant {
    rv_chrom :: !Int,                                -- chromosome index (0-based)
    rv_pos   :: !Int,                                -- position (1-based)
    rv_vars  :: !Bytes,                              -- ref, tab, vars (comma separated)
    rv_gt    :: !Word16,                             -- genotype
    rv_fp    :: Maybe FilePath }                     -- path to report in errors
        deriving (Show, Eq)


-- | Position and length to list of 'Lump's.  Used to create either 'Ns'
-- or 'Eqs2' to enable dense and sparse input, also used to create
-- possibly multiple 'Ns' and 'Eqs2' when unbreaking piecewise broken
-- input.
type GapCons = Int -> Int -> [Lump]

-- | One 'GapCons' function for each chromosome.  Usually a const
-- function, but table driven when unbreaking piecewise broken input.
type Density = Int -> GapCons

type StreamRV = Stream (Of RawVariant) IO ()

dense, sparse :: Density
dense  _ _ = pure . Ns
sparse _ _ = pure . Eqs2

data ConfXcf = ConfXcf
    { conf_output  :: FilePath
    , conf_ref     :: FilePath
    , conf_density :: RefSeqs -> StreamRV -> IO (Of Density StreamRV)
    , conf_ploidy  :: Lump -> Lump
    , conf_key     :: String
    , conf_ext     :: String
    , conf_reader  :: [Bytes] -> FilePath -> (StreamRV -> IO ()) -> IO () }

conf_vcf :: ConfXcf
conf_vcf = ConfXcf "-" (throw $ PebkacError "no reference specified")
                   detect_density id "vcfin" "vcf" readVcf

opts_xcf :: [ OptDescr ( ConfXcf -> IO ConfXcf ) ]
opts_xcf =
    [ Option "o" ["output"]        (ReqArg set_output "FILE") "Write output to FILE (.hef)"
    , Option "r" ["reference"]        (ReqArg set_ref "FILE") "Read reference from FILE (.2bit)"
    , Option "D" ["dense"]                 (NoArg  set_dense) "Input stores all sites"
    , Option "S" ["sparse"]                (NoArg set_sparse) "Input stores only variants"
    , Option ""  ["good-patches"] (ReqArg set_bed_pos "FILE") "Read hi-qual regions from FILE (.bed)"
    , Option ""  ["poor-patches"] (ReqArg set_bed_neg "FILE") "Read lo-qual regions from FILE (.bed)"
    , Option "L" ["low"]                   (NoArg    set_low) "Low coverage, stores haploid calls"
    , Option "H" ["high"]                  (NoArg   set_high) "High coverage, stores diploid calls" ]
  where
    set_output a c = return $ c { conf_output  = a }
    set_ref    a c = return $ c { conf_ref     = a }

    set_bed_pos = set_bed toDensity_positive
    set_bed_neg = set_bed toDensity_negative

    set_bed k f c = do let mkdens refs str = do
                                bed <- withFile f ReadMode $
                                            parseBed (rss_chroms refs) . S.hGetContents
                                return $ k bed :> str
                       return $ c { conf_density = mkdens }

    set_dense    c = return $ c { conf_density = const $ pure . (:>)  dense }
    set_sparse   c = return $ c { conf_density = const $ pure . (:>) sparse }
    set_low      c = return $ c { conf_ploidy  = make_hap }
    set_high     c = return $ c { conf_ploidy  = id       }


main_xcf :: ConfXcf -> [String] -> IO ()
main_xcf conf0 args = do
    ( vcfs, ConfXcf{..} ) <- let fspec = "[" ++ conf_ext conf0 ++ "-file...]"
                             in parseFileOpts conf0 (conf_key conf0) fspec opts_xcf args
    ref <- readTwoBit conf_ref
    withMany (conf_reader $ rss_chroms ref) (if null vcfs then ["-"] else vcfs) $ \inp0 -> do
            density :> inp <- conf_density ref inp0
            yenome <- Q.fold_ (\y (k :!: v) -> I.insertWith srsly k v y) I.empty id
                      . mapsM ( -- get one variant to know the chromosome, store it
                                -- together with the packed form of the whole chromosome
                                Q.next >=> \(Right (v1,s1)) ->
                                fmap (Q.mapOf (rv_chrom v1 :!:)) .
                                encodeLumpToMem . Q.map conf_ploidy $
                                importVcf (rss_chroms ref)
                                          (density $ rv_chrom v1)
                                          (rss_seqs ref !! rv_chrom v1)
                                          (Q.cons v1 s1) )
                      . Q.groupBy ((==) `on` rv_chrom)
                      . progress . dedupVcf . cleanVcf $ inp

            when (I.null yenome) $ do
                hPutStrLn stderr "Found only unexpected sequences.  Is this the right reference?"
                exitFailure

            S.writeFile conf_output . S.gzip $ do
                S.toByteStream $ encodeHeader ref
                forM_ [0 .. length (rss_chroms ref) - 1] $ \i ->
                    S.fromLazy . unpackLump $ I.findWithDefault noLump i yenome
  where
    withMany _ [ ] k = k $ pure ()
    withMany r (fp:fps) k = r fp $ \s -> withMany r fps $ k . (>>) s

    srsly _ _ = throw $ DataError Nothing "Repeated chromosome.  Is the input really sorted by coordinate?"


progress :: MonadIO m => Stream (Of RawVariant) m r -> Stream (Of RawVariant) m r
progress = go 0 0
  where
    go rs po = lift . Q.next >=> \case
      Left r -> pure r
      Right (v,vs)
        | rs /= rv_chrom v || xor po (rv_pos v) .&. complement 0x3FFFFF /= 0 -> do
                liftIO $ hPutStrLn stderr $ fromMaybe "" (rv_fp v) ++ "@" ++ show (rv_chrom v) ++ ":" ++ show (rv_pos v)
                v `Q.cons` go (rv_chrom v) (rv_pos v) vs
        | otherwise ->  v `Q.cons` go rs po vs


-- | Some idiot decided to output multiple records for the same position
-- into some VCF files.  If we hit that, we take the first.  (Einmal mit
-- Profis arbeiten!)
-- XXX  The idiocy applies to multiple SNPs at the same location.  Once
-- we support Indels, the logic in here has to change, because Indels
-- usually share the coordinate of a nearby SNP.
dedupVcf :: MonadIO m => Stream (Of RawVariant) m r -> Stream (Of RawVariant) m r
dedupVcf = lift . Q.next >=> \case Left       r  -> pure r
                                   Right (v1,vs) -> go v1 vs
  where
    go v1 = lift . Q.next >=> \case Left r                      -> v1 `Q.cons` pure r
                                    Right (v2,vs) | match v1 v2 ->             go v1 vs
                                                  | otherwise   -> v1 `Q.cons` go v2 vs

    match v1 v2 = rv_chrom v1 == rv_chrom v2 && rv_pos v1 == rv_pos v2


-- | Remove indel variants, since we can't very well use them, and
-- \"variants\" with invalid chromosome numbers, since we clearly don't
-- want them.
-- XXX  If we want to support Indel variants, this has to go.
cleanVcf :: Monad m => Stream (Of RawVariant) m r -> Stream (Of RawVariant) m r
cleanVcf = Q.filter $ \RawVariant{..} ->
    rv_chrom >= 0 &&
    ( B.length rv_vars == 1 ||
      B.length rv_vars == B.count ',' rv_vars * 2 + 3 )

-- | Imports one chromosome worth of VCF style 'RawVariant's into a
-- stream of 'Lump's with a 'Break' tacked onto the end.  This assumes
-- that all variants have the same chromsome, it's best preceeded by
-- 'Streaming.Prelude.groupBy'.  Pseudo-variants of the "no call" type
-- must be filtered beforehand, see 'cleanVcf'.

importVcf :: Monad m => [Bytes] -> GapCons -> (() -> RefSeq) -> Stream (Of RawVariant) m r -> Stream (Of Lump) m r
importVcf chroms ns frs = normalizeLump . generic 1 (frs ()) -- Start the coordinate at one, for VCF is one-based.
  where
    generic pos rs = lift . Q.next >=> \case
        Left r                    -> Q.each (ns (pos-1) (fromIntegral (lengthRS rs))) >>
                                     Q.cons Break (pure r)
        Right (var1,vars)
            -- long gap, creates Ns or Eqs2
            | rv_pos var1 > pos   -> Q.each (ns (pos-1) (rv_pos var1 - pos)) >>
                                     generic (rv_pos var1) (dropRS (rv_pos var1 - pos) rs) (var1 `Q.cons` vars)

            -- positions must match now
            | rv_pos var1 < pos   -> throw . DataError (rv_fp var1) $
                                            "Got variant position " ++ show (rv_pos var1) ++
                                            " when expecting " ++ show pos ++ " or higher."

            | B.null (rv_vars var1) -> throw . DataError (rv_fp var1) $ "No reference allele.  This VCF is broken."

            | otherwise -> case unconsRS rs of
                Just (nr, rs') | nr == char_to_2b (B.head (rv_vars var1)) ->
                     Q.cons (if isVar var1 then get_var_code var1 else Eqs2 1)
                            (generic (succ pos) rs' vars)

                _ -> throw . DataError (rv_fp var1) $
                            "Reference allele does not match actual reference at "
                            ++ unpack (chroms !! rv_chrom var1) ++ ":" ++ shows pos ".\nIs this the correct reference genome?"
              where

                -- *sigh*  Have to turn a numeric genotype into a 'Lump'.  We
                -- have characters for the variants, and we need to map a pair of
                -- them to a code.
                get_var_code RawVariant{..}
                    -- This looks wrong.  Unless we don't even get here for
                    -- reference values calls, this shouldn't be here.
                    | B.any (== 'N') rv_vars = Ns 1

                    -- missing call
                    | rv_gt == 0xFF00 || rv_gt == 0x0000 = Ns 1

                    -- haploid call or one allele missing
                    | rv_gt .&. 0xFF00 == 0xFF00 || rv_gt .&. 0xFF00 == 0x0000
                                = encOneVar  n0 c1

                    -- diploid call
                    | otherwise = encTwoVars n0 c1 c2
                  where
                    v1 = fromIntegral $ rv_gt            .&. 0x00FE - 2
                    v2 = fromIntegral $ rv_gt `shiftR` 8 .&. 0x00FE - 2

                    n0 = char_to_2b $ B.head rv_vars
                    c1 = char_to_2b $ safeIndex "c1" rv_vars v1
                    c2 = char_to_2b $ safeIndex ("c2 "++shows rv_pos " " ++ showHex rv_gt " ") rv_vars v2

                safeIndex m s i | B.length s > i = B.index s i
                                | otherwise = throw . DataError (rv_fp var1) $
                                                    "Attempted to index " ++ shows i " in " ++ shows s " (" ++ m ++ ")."

                char_to_2b 'T' = N2b 0
                char_to_2b 't' = N2b 0
                char_to_2b 'C' = N2b 1
                char_to_2b 'c' = N2b 1
                char_to_2b 'A' = N2b 2
                char_to_2b 'a' = N2b 2
                char_to_2b 'G' = N2b 3
                char_to_2b 'g' = N2b 3
                char_to_2b  c  = throw . DataError (rv_fp var1) $ "What's a " ++ shows c "?"

                isVar RawVariant{..} | rv_gt            == 0xFF02 = False     -- "0"
                                     | rv_gt .&. 0xFCFE == 0x0002 = False     -- "0|.", "0/.", "0|0", "0/0"
                                     | otherwise                  = True


-- | Trying to detect density.  A sequence of entries for, say, 128
-- consecutive positions is a pretty strong tell that the stream is
-- dense.  Otherwise, a gap of, say, 32 positions where the reference
-- does not also have a gap, is a pretty strong tell it is sparse.  Just
-- by looking at the first couple thousand records, we know.  Else, the
-- command line options are still there.
detect_density :: RefSeqs -> StreamRV -> IO (Of Density StreamRV)
detect_density ref =
    Q.toList . Q.splitAt 2048 >=> \(vars :> rest) ->
    if is_dense vars then do
        hPutStrLn stderr "Hmm, this looks like a dense data set."
        return (dense :> (Q.each vars >> rest))
    else if is_sparse vars then do
        hPutStrLn stderr "Hmm, this looks like a sparse data set."
        return (sparse :> (Q.each vars >> rest))
    else do
        hPutStrLn stderr "Hmm, this data set does not make sense to me."
        hPutStrLn stderr "Specify either --dense or --sparse to import it correctly."
        exitFailure
  where
    is_dense = is_dense' 0 0 (0::Int)

    is_dense' !_ !_ 128 _ = True
    is_dense' !_ !_ !_ [] = False
    is_dense' !c !p !n (v:vs) | rv_chrom v /= c = is_dense' (rv_chrom v) (rv_pos v)   1   vs
                              | rv_pos v == p   = is_dense' (rv_chrom v) (rv_pos v)   n   vs
                              | rv_pos v == p+1 = is_dense' (rv_chrom v) (rv_pos v) (n+1) vs
                              | otherwise       = is_dense' (rv_chrom v) (rv_pos v)   1   vs

    is_sparse [    ] = False
    is_sparse (v:vs) = is_sparse' (find_ref $ rv_chrom v) (rv_chrom v) 0 (0::Int) (v:vs)

    is_sparse' _ !_ !_ 32   _         = True
    is_sparse' _ !_ !_ !_ [    ]      = False
    is_sparse' r !c !p !n (v:vs)
        | rv_chrom v /= c             = is_sparse (v:vs)
        | rv_pos v < p                = is_sparse' r c p n vs
        | otherwise = case viewRS r of
            NilRef                   -> is_sparse' r  c (rv_pos v)  n  vs
            _ :-- r'                 -> is_sparse' r' c   p     n   (v:vs)
            l :== r'                 -> is_sparse' r' c (p+l)   0   (v:vs)
            _ :^  r' | rv_pos v == p -> is_sparse' r' c (p+1)   0      vs
                     | otherwise     -> is_sparse' r' c (p+1) (n+1) (v:vs)

    find_ref ix = case drop ix $ rss_seqs ref of s:_ -> s () ; [] -> RefEnd


toDensity_positive :: Bed -> Int -> Int -> Int -> [Lump]
toDensity_positive = toDensity (Eqs2 . fromIntegral) (Ns . fromIntegral)

toDensity_negative :: Bed -> Int -> Int -> Int -> [Lump]
toDensity_negative = toDensity (Ns . fromIntegral) (Eqs2 . fromIntegral)

toDensity :: (Int32 -> Lump) -> (Int32 -> Lump) -> Bed -> Int -> Int -> Int -> [Lump]
toDensity eqs ns (Bed vee) rs p0 l0
    = go (fromIntegral p0) (fromIntegral $ p0+l0)
    . U.toList . U.drop (binSearch 0 (U.length vee -1)) $ vee
  where
    r0  = fromIntegral rs

    go s e rgns@((r1,s1,e1):rgns')
        | r1 == r0 && s1 < e = case () of
            _ | s1 <= s && e <= e1 -> eqs  (e-s) : []               -- done, contained in HQ rgn
              | s1 <= s && s <  e1 -> eqs (e1-s) : go e1 e rgns'    -- overlap left
              | s1 > s             -> ns  (s1-s) : go s1 e rgns     -- overlap right
              | otherwise          ->              go s  e rgns'    -- shouldn't happen
    go s e _                        = ns   (e-s) : []               -- done, contained in LQ rgn

    -- finds the first entry that could overlap [p0,p0+l0)
    binSearch u v
        | u >= v    = u
        | bigger    = binSearch   u   m
        | otherwise = binSearch (m+1) v
      where
        m         = (u+v) `div` 2      -- ensures m >= u && m < v
        (r1,_,e1) = vee U.! m
        bigger    = r1 == r0 && e1 > fromIntegral p0 || r1 > r0


-- | pure Haskell version of VCF reader
readVcf :: [Bytes] -> FilePath -> (Stream (Of RawVariant) IO () -> IO r) -> IO r
readVcf chroms fp k =
    withInputFile fp $
        k . Q.map parse_rec
          . Q.dropWhile (\s -> B.null s || B.head s == '#')
          . S.lines' . S.decomp . S.hGetContents
  where
    !ctab = M.fromList $ zip chroms [0..]

    parse_rec :: Bytes -> RawVariant
    parse_rec s0 =
        case getField     s0 of { (!chrom, !s1) ->
        case getInt       s1 of { (!pos,   !s2) ->
        case dropField    s2 of { !s3           ->  -- id
        case get2Field    s3 of { (!vars, !s5)  ->
        case dropField    s5 of { !s6           ->  -- qual
        case dropField    s6 of { !s7           ->  -- filter
        case dropField    s7 of { !s8           ->  -- info
        case dropField    s8 of { !s9           ->  -- format
        case getAllele  0 s9 of { (!x, !s10)    ->
        case getAllele2  s10 of { (!y,!_s11)    ->
        RawVariant {
            rv_chrom = M.lookupDefault (-1) chrom ctab,
            rv_pos   = pos,
            rv_vars  = vars,
            rv_gt    = fromIntegral $ x .|. shiftL y 8,
            rv_fp    = Just fp
        }}}}}}}}}}}
      where
        getField  s = case B.elemIndex '\t' s of Just  i -> (B.take i s, B.drop (i+1) s)
                                                 Nothing -> (s, B.empty)

        get2Field s = case B.elemIndex '\t' s of
                        Just  i -> case B.elemIndex '\t' (B.drop (i+1) s) of
                            Just  j ->
                                if j == 1 && B.index s (i+1) == '.'
                                    then (B.take i s, B.drop (i+2+j) s) -- no alt alleles
                                    else (B.take (i+1+j) s, B.drop (i+2+j) s)
                            Nothing -> (B.take i s, B.empty)
                        Nothing -> (s, B.empty)

        dropField s = case B.elemIndex '\t' s of Just  i -> B.drop (i+1) s
                                                 Nothing -> B.empty

        getInt    s = case B.readInt s of Just (i,s') -> (i,B.drop 1 s')
                                          Nothing -> throw . DataError (Just fp) $ "pos?!" ++ show (s0,s)

        getAllele a s = case B.readInt s of Just (i,s') -> (2*(i+1)+a,s')
                                            Nothing     -> case B.uncons s of Just ('.',s') -> (0,s')
                                                                              _ -> throw . DataError (Just fp) $ "GT?! " ++ show (s0,s)

        getAllele2 s = case B.uncons s of Just ('|',s') -> getAllele 1 s'
                                          Just ('/',s') -> getAllele 0 s'
                                          _             -> (0xff,s)
