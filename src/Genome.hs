{-# LANGUAGE CPP #-}
module Genome where

-- ^ We'll use a 2bit file as reference.  Chromosomes in the file will
-- be used in exactly this order, everything else is ignored.
--
-- When reading input, we always supply the reference file.  Sometimes we
-- don't actually need the reference sequence, but we still use it to
-- decide which chromomes to include, and sometimes how to reorder them.
-- We also compute a checksum over the list of target chromosomes and
-- leave it in the hef files themselves; that way, we can ensure that
-- two hefs can be merged safely.
--
-- Applications for import:
--
--   VCF, BCF files:  A bunch of files, usually in the wrong order.  We
--                    read each, split them into chromosomes, then use
--                    the 2bit file to put them in the correct order.
--
--   BAM files:  Must be sorted, but the order of target sequences is
--               arbitrary.  We require the reference, and reorder valid
--               chromosomes after we're done importing.
--
--   EMF, MAF:  Will not be sorted.  We parse pieces in arbitrary order,
--              and require the reference so we can put everything into
--              a proper order at the end.
--
-- Applications for export:
--
--   D-stats, Treemix:  The reference is no longer needed!
--
--   Eigenstrat:  Can be done without the reference, but will look
--                ridiculous.  Output of proper SNP files requires the
--                reference.
--
--   VCF:  Clearly requires the reference.


import Bio.Prelude                   hiding ( Ns )
import Bio.Util.MMap                        ( unsafeMMapFile )
import Data.ByteString.Short                ( ShortByteString, toShort )
import Data.Vector.Storable                 ( Vector )
import Paths_heffalump                      ( getDataDir )
import Streaming                            ( Stream, Of(..), MonadIO(..) )
import System.Console.GetOpt
import System.Directory                     ( makeAbsolute, doesFileExist )
import System.FilePath                      ( takeFileName, takeDirectory, makeRelative, splitSearchPath, (</>) )

import qualified ByteStream                         as S
import qualified Data.ByteString                    as B
import qualified Data.ByteString.Builder            as B
import qualified Data.ByteString.Char8              as C
import qualified Data.ByteString.Internal           as B
import qualified Data.ByteString.Lazy.Char8         as L
import qualified Data.ByteString.Short              as H
import qualified Data.ByteString.Unsafe             as B
import qualified Data.HashSet                       as Hash
import qualified Streaming.Prelude                  as Q

import Util ( parseFileOpts, withInputFile, withOutputFile, DataError(..) )

-- | This is a reference sequence.  It consists of stretches of Ns and
-- stretches of sequence.  Invariant:  the lengths for 'ManyNs' and
-- 'SomeSeq' are always strictly greater than zero.
--
-- The offset is stored as a 'Word'.  This is done because on a 32 bit
-- platform, every bit counts.  This limits the genome to approximately
-- four gigabases, which would be a file of about one gigabyte.  That's
-- just about enough to work with the human genome.

data RefSeq = ManyNs !Int RefSeq
              -- Primitive bases in 2bit encoding:  [0..3] = TCAG
            | SomeSeq {-# UNPACK #-} !B.ByteString  -- ^ underlying data
                      {-# UNPACK #-} !Word          -- ^ offset in bases(!)
                      {-# UNPACK #-} !Int           -- ^ length in bases
                      RefSeq
            | RefEnd

instance Show RefSeq where
    showsPrec _ (ManyNs      n r) = (++) "ManyNs " . shows n . (++) " $ " . shows r
    showsPrec _ (SomeSeq _ _ l r) = (++) "SomeSeq " . shows l . (++) " $ " . shows r
    showsPrec _  RefEnd           = (++) "RefEnd"

data RefSeqs = RefSeqs { rss_chroms  :: [ B.ByteString ]
                       , rss_lengths :: [ Int ]
                       , rss_seqs    :: [ () -> RefSeq ]
                       , rss_path    :: !B.ByteString }

readTwoBit :: FilePath -> IO RefSeqs
readTwoBit fp = parseTwoBit <$> makeAbsolute fp <*> unsafeMMapFile fp

-- In theory, there could be 2bit files in big endian format out there.
-- We don't support them, since I've never seen one in the wild.
parseTwoBit :: FilePath -> B.ByteString -> RefSeqs
parseTwoBit fp0 raw = case (getW32 0, getW32 4) of (0x1A412743, 0) -> parseEachSeq 16 0
                                                   _ -> throw $ DataError (Just fp0) "This does not look like a 2bit file."
  where
    getW32 :: Int -> Word32
    getW32 o | o + 4 >= B.length raw = throw $ DataError (Just fp0) ("out of bounds access: " ++ show (o, B.length raw))
             | otherwise =
#if MIN_VERSION_bytestring(0,10,8)
                B.accursedUnutterablePerformIO $
#else
                B.inlinePerformIO $
#endif
                B.unsafeUseAsCString raw $ \p -> peekByteOff p o

    num_seq  = getW32 8

    parseEachSeq  _  n | num_seq == n = RefSeqs [] [] [] (fromString fp0)
    parseEachSeq off n = case parseEachSeq (off+5+nmsize) (n+1) of
                            RefSeqs cs ls ss fp ->
                                RefSeqs (name:cs) (fromIntegral dnasize:ls) ((\() -> the_seq):ss) fp
      where
        !nmsize  = fromIntegral $ B.index raw off
        !name    = B.take nmsize $ B.drop (1+off) raw
        !offset  = fromIntegral . getW32 $ off+1+nmsize

        !dnasize        = fromIntegral . getW32 $ offset
        !nBlockCount    = fromIntegral . getW32 $ offset + 4
        !maskBlockCount = fromIntegral . getW32 $ offset + 8 + 8*nBlockCount
        !packedDnaOff   = offset + 16 + 8 * (nBlockCount+maskBlockCount)

        -- We will need to decode the N blocks, but we ignore the M blocks.
        n_blocks = [ (u,v) | i <- [ 0 .. nBlockCount-1 ]
                           , let u = fromIntegral . getW32 $ offset+8 + 4*i
                           , let v = fromIntegral . getW32 $ offset+8 + 4*(i+nBlockCount) ]

        the_seq = unfoldSeq dnasize n_blocks 0 (fromIntegral packedDnaOff * 4)


    unfoldSeq :: Word -> [(Word,Word)] -> Word -> Word -> RefSeq
    unfoldSeq dnasize  _ chroff _fileoff | chroff >= dnasize = RefEnd
    unfoldSeq dnasize [] chroff  fileoff = SomeSeq raw fileoff (fromIntegral $ dnasize-chroff) RefEnd

    unfoldSeq dnasize ((nstart,nlen):ns) chroff fileoff

        | chroff  < nstart = SomeSeq raw fileoff (fromIntegral $ nstart-chroff)
                           $ unfoldSeq dnasize ((nstart,nlen):ns) nstart (fileoff+nstart-chroff)

        | chroff == nstart = ManyNs (fromIntegral nlen)
                           $ unfoldSeq dnasize ns (chroff+nlen) (fileoff+nlen)

        | otherwise = throw $ DataError (Just fp0) "broken 2bit file"


{-# INLINE lengthRS #-}
lengthRS :: RefSeq -> Int64
lengthRS = go 0
  where
    go !acc (ManyNs      l s) = go (acc + fromIntegral l) s
    go !acc (SomeSeq _ _ l s) = go (acc + fromIntegral l) s
    go !acc  RefEnd           = acc

{-# INLINE nullRS #-}
nullRS :: RefSeq -> Bool
nullRS = go
  where
    go (ManyNs      0 s) = go s
    go (SomeSeq _ _ 0 s) = go s
    go  RefEnd           = True
    go  _                = False

{-# INLINE dropRS #-}
dropRS :: Int -> RefSeq -> RefSeq
dropRS 0                s              = s
dropRS _  RefEnd                       = RefEnd
dropRS n (ManyNs      l s) | n > l     = dropRS (n-l) s
                           | n < l     = ManyNs (l-n) s
                           | otherwise = s
dropRS n (SomeSeq r o l s) | n > l     = dropRS (n-l) s
                           | n < l     = SomeSeq r (o + fromIntegral n) (l-n) s
                           | otherwise = s

{-# INLINE takeRS #-}
takeRS :: Int -> RefSeq -> RefSeq
takeRS 0                _              = RefEnd
takeRS _  RefEnd                       = RefEnd
takeRS n (ManyNs      l s) | n >= l    = ManyNs l $ takeRS (n-l) s
                           | otherwise = ManyNs n RefEnd
takeRS n (SomeSeq r o l s) | n <= l    = SomeSeq r o n RefEnd
                           | otherwise = SomeSeq r o l $ takeRS (n-l) s

unpackRS :: RefSeq -> L.ByteString
unpackRS = L.unfoldr $ fmap (first toCode) . unconsRS
  where
    toCode (N2b 0) = 'T'
    toCode (N2b 1) = 'C'
    toCode (N2b 2) = 'A'
    toCode (N2b 3) = 'G'
    toCode      _  = 'N'

-- | Nucleotide in 2bit encoding: "TCAG" == [0..3], anything else is N
newtype Nuc2b = N2b Word8 deriving Eq

-- | Variant in 2bit encoding: [0..3] for "IOPX" (Identity, transitiOn,
-- comPlement, X-compl)
newtype Var2b = V2b { unV2b :: Word8 } deriving Eq

isTransversion :: Var2b -> Bool
isTransversion (V2b v) = testBit v 1

-- | Applies a variant to a nucleotide, resulting in a new nucleotide.
appVar :: Nuc2b -> Var2b -> Nuc2b
appVar (N2b r) (V2b a) = N2b $ xor r a

-- | Encodes the alternative allele as text: \'.\' for unknown, \"TCAG\" if
-- known, and \"IOPX\" if the type of variant (Identity, transitiOn,
-- comPlement, Xcomplement) is known but the reference isn't.
toAltCode :: Var2b -> Nuc2b -> Char
toAltCode (V2b v) (N2b r)
    | v >= 4    = '.'
    | r >= 4    = C.index "IOPX" $ fromIntegral (xor r v .&. 3)
    | otherwise = C.index "TCAG" $ fromIntegral (xor r v)

-- | Encodes the reference allele as text, one of "TCAGN".
toRefCode :: Nuc2b -> Char
toRefCode (N2b r) | r >= 4 = 'N'
toRefCode (N2b r)          = C.index "TCAG" $ fromIntegral r

isKnown :: Nuc2b -> Bool
isKnown (N2b r) = r < 4

instance Show Nuc2b where
    show (N2b 0) = "T"
    show (N2b 1) = "C"
    show (N2b 2) = "A"
    show (N2b 3) = "G"
    show      _  = "N"

instance Show Var2b where
    show (V2b 0) = "I"
    show (V2b 1) = "O"
    show (V2b 2) = "P"
    show (V2b 3) = "X"
    show      _  = "?"

{-# INLINE unconsRS #-}
unconsRS :: RefSeq -> Maybe ( Nuc2b, RefSeq )
unconsRS  RefEnd                 = Nothing
unconsRS (ManyNs            1 s) = Just (N2b 255, s)
unconsRS (ManyNs            l s) = Just (N2b 255, ManyNs (l-1) s)
unconsRS (SomeSeq raw off len s) = Just (c, s')
  where
    c  = N2b . fromIntegral $ (B.index raw (fromIntegral $ off `shiftR` 2) `shiftR` (6 - 2 * (fromIntegral $ off .&. 3))) .&. 3
    s' = if len == 1 then s else SomeSeq raw (off+1) (len-1) s


-- | A way to decompose a reference sequence.  Actual references (2bit
-- files) will not have gaps, but the reference implicit in Emf and Maf
-- files can.
data RefSeqView a = !Int :== a   -- ^ a stretch of Ns
                  | !Int :-- a   -- ^ a gap
                  | !Nuc2b :^ a  -- ^ a nucleotide
                  | NilRef       -- ^ the end

viewRS :: RefSeq -> RefSeqView RefSeq
viewRS  RefEnd                 = NilRef
viewRS (ManyNs            l s) = l :== s
viewRS (SomeSeq raw off len s) = c :^  s'
  where
    c  = N2b . fromIntegral $ (B.index raw (fromIntegral $ off `shiftR` 2) `shiftR` (6 - 2 * (fromIntegral off .&. 3))) .&. 3
    s' = if len == 1 then s else SomeSeq raw (off+1) (len-1) s

data AlleleCounts = AC { ac_num_ref :: !Word8, ac_num_alt :: !Word8 } deriving Eq

-- Compact storage:  both counts fit into one 'Word8'.
instance Storable AlleleCounts where
    sizeOf    _ = 1
    alignment _ = 1
    poke p (AC r a) = poke (castPtr p) (r + shiftL a 2)
    peek p = (\w -> AC (w .&. 3) (shiftR w 2)) <$> peek (castPtr p)

instance Show AlleleCounts where
    showsPrec _ (AC a b) = shows a . (:) '/' . shows b

-- | A strict pair of 'Int's.  Comparable to 'AlleleCounts', but not
-- prone to overflow if we tally over large (>85) numbers of
-- individuals.  Still looking for a better name.
data I2 = I2 !Int !Int

addAC :: I2 -> AlleleCounts -> I2
addAC (I2 a b) (AC u v) = I2 (a + fromIntegral u) (b + fromIntegral v)

instance Storable I2 where
    sizeOf    ~(I2 a b) = sizeOf a + sizeOf b
    alignment ~(I2 a _) = alignment a
    poke     p (I2 a b) = pokeElemOff (castPtr p) 0 a >> pokeElemOff (castPtr p) 1 b
    peek     p   = I2 <$> peekElemOff (castPtr p) 0  <*> peekElemOff (castPtr p) 1


-- A variant call.  Has a genomic position, ref and alt alleles, and a
-- bunch of calls expressed as allele counts.
data Variant = Variant { v_chr   :: !Int                -- chromosome number
                       , v_pos   :: !Int                -- 0-based
                       , v_ref   :: !Nuc2b
                       , v_alt   :: !Var2b
                       , v_calls :: !(Vector AlleleCounts) }
  deriving Show


has_help :: [String] -> Bool
has_help args = null args || any (`elem` h_opts) (takeWhile (/= "--") args)
  where
    h_opts = ["-h","--help","-?","--usage"]

eff_args :: [String] -> [String]
eff_args args = case break (== "--") args of (l,r) -> l ++ drop 1 r

main_2bitinfo :: [String] -> IO ()
main_2bitinfo fs
    | has_help fs = do
        pn <- getProgName
        hPutStrLn stderr $ "Usage: " ++ pn ++ " twobitinfo [2bit-file...]\n"
        exitFailure

    | otherwise = forM_ (eff_args fs) $ \f -> do
        ref <- readTwoBit f
        zipWithM_ (printf "%s\t%d\n" . C.unpack)
                  (rss_chroms ref) (rss_lengths ref)

main_2bittofa :: [String] -> IO ()
main_2bittofa fs = case eff_args fs of
    _ | has_help fs -> do
            pn <- getProgName
            hPutStrLn stderr $ "Usage: " ++ pn ++
                " twobittofa [2bit-file] [[name|name:start-end]...]\n"
            exitFailure

    [] -> return () -- can't happen

    [fp] -> do ref <- readTwoBit fp
               forM_ (rss_chroms ref `zip` rss_seqs ref) $ \(ch,sq) ->
                    putStrLn ('>' : C.unpack ch) >> twoBitToFa (sq ())

    (fp:rns) -> do ref <- readTwoBit fp
                   forM_ rns $ \rn ->
                        case elemIndex (C.pack rn) (rss_chroms ref) of
                            Just i  -> do putStrLn $ '>' : rn
                                          twoBitToFa $ (rss_seqs ref !! i) ()
                            Nothing -> sequence_
                                          [ do printf ">%s:%d-%d\n" ch start end
                                               twoBitToFa $ dropRS start $ takeRS end $ (rss_seqs ref !! i) ()
                                          | (ch,':':s1)    <- [break (':' ==) rn]
                                          , (start,'-':s2) <- reads s1
                                          , (end,"")       <- reads s2
                                          , i              <- elemIndices (C.pack ch) (rss_chroms ref) ]

twoBitToFa :: RefSeq -> IO ()
twoBitToFa = splitLns . unpackRS
  where
    splitLns s
        | L.null s = return ()
        | otherwise = case L.splitAt 50 s of { (l,r) -> do
                            L.hPut stdout l
                            L.hPut stdout "\n"
                            splitLns r }

opts_fato2bit :: [ OptDescr (FilePath -> IO FilePath) ]
opts_fato2bit = [ Option "o" ["output"] (ReqArg (\a _ -> return a) "FILE") "Write output to FILE" ]

main_fato2bit :: [String] -> IO ()
main_fato2bit args = do
    ( fs, fp ) <- parseFileOpts "-" "fatotwobit" "[fasta-file...]" opts_fato2bit args

    withFiles (if null fs then ["-"] else fs) $ \hs -> do
        raw <- faToTwoBit hs
        withOutputFile fp $ flip B.hPutBuilder raw
  where
    withFiles [    ] k = k $ pure ()
    withFiles (f:fs) k = withInputFile f $ \h ->
                           withFiles fs $ \s ->
                             k $ S.decomp (S.hGetContents h) >> s


main_vcfto2bit :: [String] -> IO ()
main_vcfto2bit args = do
    ( fs, fp ) <- parseFileOpts "-" "vcftotwobit" "[vcf-file...]" opts_fato2bit args

    withFiles (if null fs then ["-"] else fs) $ \hs -> do
        raw <- vcfToTwoBit hs
        withOutputFile fp $ flip B.hPutBuilder raw
  where
    withFiles [    ] k = k $ pure ()
    withFiles (f:fs) k = withInputFile f $ \h ->
                           withFiles fs $ \s ->
                             k $ S.lines' (S.decomp (S.hGetContents h)) >> s

-- List of pairs of 'Word32's.  Specialized and unpacked to conserve
-- space.  Probably overkill...
data L2i = L2i {-# UNPACK #-} !Word32 {-# UNPACK #-} !Word32 L2i | L2i_Nil

sum_L2i :: Word32 -> Pair Word32 L2i -> Word32
sum_L2i p (q :!: xs) = go (if q == maxBound then 0 else p-q) xs
  where
    go !a (L2i x y z) = go (a+y-x) z
    go !a  L2i_Nil    = a

encodeL2i :: L2i -> B.Builder
encodeL2i = go 0 mempty mempty
  where
    go !n ss ls  L2i_Nil     = B.word32LE n <> ss <> ls
    go !n ss ls (L2i s e rs) = go (succ n) (B.word32LE s <> ss) (B.word32LE (e-s) <> ls) rs


seqs_to_twobit :: [(ShortByteString, L.ByteString)] -> B.Builder
seqs_to_twobit seqs = B.word32LE 0x1A412743 <> B.word32LE 0 <>
                      B.word32LE (fromIntegral $ length seqs) <> B.word32LE 0 <>
                      fold (zipWith (\nm off -> B.word8 (fromIntegral (H.length nm)) <>
                                                B.shortByteString nm <>
                                                B.word32LE (fromIntegral off))
                                    (map fst seqs) offsets) <>
                      foldMap (B.lazyByteString . snd) seqs
  where
    offset0 = 16 + 5 * length seqs + sum (map (H.length . fst) seqs)
    offsets = scanl (\a b -> a + fromIntegral (L.length b)) offset0 $ map snd seqs



-- Strategy:  We can only write the packedDNA after we wrote the nBlocks
-- and mBlocks.  So packedDNA needs to be buffered.  We have to do three
-- simultaneous strict folds of the input, all of which result in reasonably
-- compact structures, which get concatenated at the end.
--
-- We also have to buffer everything, since the header with the sequence
-- names must be written first.  Oh joy.

faToTwoBit :: MonadIO m => S.ByteStream m r -> m B.Builder
faToTwoBit = liftM seqs_to_twobit . get_each [] . S.decomp
  where
    get_each acc = S.nextByte . S.dropWhile (/= (c2w '>')) >=> \case
                            Left    _    -> return $ reverse acc
                            Right (_,s2) -> do
                                nm :> s' <- S.toStrict $ S.break (<= 32) s2
                                get_one acc (toShort nm) 0 (maxBound :!: L2i_Nil)
                                        (maxBound :!: L2i_Nil) (0 :!: 0 :!: emptyAccu)
                                        (S.dropWhile (/= (c2w '\n')) s')

    get_one acc !nm !pos !ns !ms !bs = S.nextByte >=> \case
        Left    r       -> fin (pure r)
        Right (c,s')
            | c <= 32      -> get_one acc nm pos ns ms bs s'
            | c == c2w '>' -> fin (S.cons c s')
            | otherwise    -> get_one acc nm (succ pos)
                                      (collect_Ns ns pos c)
                                      (collect_ms ms pos $ w2c c)
                                      (collect_bases bs $ w2c c) s'
      where
        fin k = do let !r = encode_seq pos ns ms bs
                   liftIO $ hPrintf stderr "%s: %d kbases (%d Ns), %dkB encoded\n"
                                    (show nm) (pos `div` 1000) (sum_L2i pos ns) (L.length r `div` 1024)
                   get_each ((nm,r):acc) k


-- | Extracts the reference from a VCF.  This assumes the presence of at
-- least one record per site.  The VCF must be sorted by position.  When
-- writing out, we y to match the order of the contigs as listed in the
-- header.  Unlisted contigs follow at the end with their order
-- preserved; contigs without data are not written at all.
vcfToTwoBit :: MonadIO m => Stream (Of Bytes) m r -> m B.Builder
vcfToTwoBit s0 = do lns :> s1 <- read_header [] s0
                    seqs <- get_each lns [] $
                        Q.filter (\s -> not (B.null s) && C.head s /= '#') s1
                    return $ seqs_to_twobit $ reorder (map fst lns) seqs
  where
    -- Collects the "contig" stanzas, parses their lengths.  Returns the
    -- length map and the remaining stream.
    read_header acc = Q.next >=> \case
        Left r                                               -> return $ reverse acc :> pure r
        Right (l,ls) | "##contig=" `C.isPrefixOf` l
                     , (Just !nm, Just !ln) <- parse_cline l -> read_header ((nm,ln):acc) ls
                     | "#" `C.isPrefixOf` l                  -> read_header acc ls
                     | otherwise                             -> return $ reverse acc :> Q.cons l ls

    parse_cline = p1 . C.filter (not . isSpace) . C.takeWhile (/='>') . C.drop 1 . C.dropWhile (/='<')
      where
        p1 s | "ID=" `C.isPrefixOf` s = let (nm,t) = C.break (==',') $ C.drop 3 s
                                            (_,ln) = p1 $ C.drop 1 t
                                        in (Just (toShort nm),ln)

             | "length=" `C.isPrefixOf` s = case C.readInt $ C.drop 7 s of
                    Just (ln,u) -> let (nm,_) = p1 $ C.drop 1 $ C.dropWhile (/=',') u in (nm,Just (fromIntegral ln))
                    Nothing     -> p1 $ C.drop 1 $ C.dropWhile (/=',') s

             | C.null s = (Nothing,Nothing)
             | otherwise = p1 $ C.drop 1 $ C.dropWhile (/=',') s

    get_each :: MonadIO m
             => [(ShortByteString,Word32)]
             -> [(ShortByteString, L.ByteString)]
             -> Stream (Of Bytes) m r
             -> m [(ShortByteString, L.ByteString)]
    get_each lns acc = Q.next >=> \case
                            Left    _    -> return $ reverse acc
                            Right (l,s2) -> do
                                let nm = B.takeWhile (/= 9) l
                                (pos,ns,bs) :> s3 <- get_one nm 0 (maxBound :!: L2i_Nil)
                                                             (0 :!: 0 :!: emptyAccu) (Q.cons l s2)
                                let !nm' = toShort nm
                                    (ns',bs',ln') =
                                        case find ((==) nm' . fst) lns of
                                            Just (_,ln) | ln > pos ->
                                                 (extend_gap ns ln, pad_bases bs (fromIntegral $ ln-pos), ln)
                                            _ -> (ns,bs,pos)
                                    !r = encode_seq ln' ns' (maxBound :!: L2i_Nil) bs'

                                liftIO $ hPrintf stderr "%s: %d kbases (%d Ns), %dkB encoded\n"
                                            (show nm') (ln' `div` 1000) (sum_L2i ln' ns') (L.length r `div` 1024)

                                L.length r `seq` get_each lns ((nm',r):acc) s3

    -- important: 1-based coordinates!
    get_one !nm !pos !ns !bs = Q.next >=> \case
        Left    r                           -> return $ (pos,ns,bs) :> pure r
        Right (l,s')
            | B.takeWhile (/=9) l /= nm     -> return $ (pos,ns,bs) :> Q.cons l s'

            | Just (pos',l3) <- C.readInt . C.drop 1 $ B.dropWhile (/=9) l
            , ref <- B.takeWhile (/=9) . B.drop 1 . B.dropWhile (/=9) $ B.drop 1 l3
            , fromIntegral pos' >= pos + 1
            , not (C.null ref) ->
                if fromIntegral pos' == pos + 1
                    -- record in sequence
                    then get_one nm (succ pos) (collect_Ns ns pos $ B.head ref)
                                               (collect_bases bs  $ C.head ref) s'
                    -- gap:  handle the gap, reprocess the record
                    else let gap_len = pos' - fromIntegral pos - 1
                         in get_one nm (fromIntegral pos' - 1) (extend_gap ns pos)
                                       (pad_bases bs gap_len) (Q.cons l s')

            -- anything else can be ignored (parse errors or additional records)
            | otherwise                     -> get_one nm pos ns bs s'


    pad_bases bs n = foldl' collect_bases bs $ replicate n 'T'

    -- Reorder a key-value list so it matches the order of a list of
    -- keys.  Missing keys are ignored, leftover pairs retain their
    -- original order.
    reorder :: Eq a => [a] -> [(a,b)] -> [(a,b)]
    reorder [    ] vs = vs
    reorder (k:ks) vs = go [] vs
      where
        go xs ((k1,v1):ys) | k  ==  k1 = (k1,v1) : reorder ks (reverse xs ++ ys)
                           | otherwise = go ((k1,v1):xs) ys
        go xs [          ]             = reorder ks (reverse xs)


encode_seq :: Word32                                    -- ^ length
           -> Pair Word32 L2i                           -- ^ list of N stretches
           -> Pair Word32 L2i                           -- ^ list of mask stretches
           -> Pair (Pair Int Word8) Accu                -- ^ accumulated bases
           -> L.ByteString

encode_seq pos ns ms bs = L.length r `seq` r
  where
    ss' = case bs of (0 :!: _ :!: ss) -> ss
                     (n :!: w :!: ss) -> grow (w `shiftL` (8-2*n)) ss
    r = B.toLazyByteString $
              B.word32LE pos <>
              encodeL2i (case ns of p :!: rs | p == maxBound -> rs ; p :!: rs -> L2i p pos rs) <>
              encodeL2i (case ms of p :!: rs | p == maxBound -> rs ; p :!: rs -> L2i p pos rs) <>
              B.word32LE 0 <>
              buildAccu ss'


-- | Collects stretches of Ns by looking at one character at a time.  In
-- reality, anything that isn't one of \"ACGT\" is treated as an N.
collect_Ns :: Pair Word32 L2i -> Word32 -> Word8 -> Pair Word32 L2i
collect_Ns (spos :!: rs) pos c
    | spos == maxBound && c `B.elem` "ACGTacgt" = maxBound :!: rs
    | spos == maxBound                          =      pos :!: rs
    |                     c `B.elem` "ACGTacgt" = maxBound :!: L2i spos pos rs
    | otherwise                                 =     spos :!: rs

extend_gap :: Pair Word32 L2i -> Word32 -> Pair Word32 L2i
extend_gap (spos :!: rs) pos
    | spos == maxBound                          =      pos :!: rs
    | otherwise                                 =     spos :!: rs

-- | Collects stretches of masked dna by looking at one letter at a
-- time.  Anything lowercase is considered masked.
collect_ms :: Pair Word32 L2i -> Word32 -> Char -> Pair Word32 L2i
collect_ms (spos :!: rs) pos c
    | spos == maxBound && isUpper c = maxBound :!: rs
    | spos == maxBound              =      pos :!: rs
    |                     isUpper c = maxBound :!: L2i spos pos rs
    | otherwise                     =     spos :!: rs

-- | Collects bases in 2bit format.  It accumulates 4 bases in one word,
-- then collects bytes in an 'Accu'.  From the 2bit spec:
--
-- packedDna - the DNA packed to two bits per base, represented as
--             so: T - 00, C - 01, A - 10, G - 11. The first base is
--             in the most significant 2-bit byte; the last base is
--             in the least significant 2 bits. For example, the
--             sequence TCAG is represented as 00011011.
collect_bases :: Pair (Pair Int Word8) Accu -> Char -> Pair (Pair Int Word8) Accu
collect_bases (n :!: w :!: ss) c
    = let code = case c of 'C'->1;'c'->1;'A'->2;'a'->2;'G'->3;'g'->3;_->0
          w'   = shiftL w 2 .|. code
      in if n == 3 then 0 :!: 0 :!: grow w' ss else succ n :!: w' :!: ss

-- | A way to accumulate bytes.  If the accumulated bytes will hang
-- around in memory, this has much lower overhead than 'Builder'.  If it
-- has short lifetime, 'Builder' is much more convenient.
newtype Accu = Accu [B.ByteString]

emptyAccu :: Accu
emptyAccu = Accu []

-- | Appends bytes to a collection of 'ByteString's in such a
-- way that the 'ByteString's keep doubling in size.  This ensures O(n)
-- time and space complexity and fairly low overhead.
grow :: Word8 -> Accu -> Accu
grow w = go 1 [B.singleton w]
  where
    go l acc (Accu (s:ss))
        | B.length s <= l  = go (l+B.length s) (s:acc) (Accu ss)
        | otherwise        = let !s' = B.concat acc in  Accu (s' : s : ss)
    go _ acc (Accu [    ]) = let !s' = B.concat acc in  Accu [s']

buildAccu :: Accu -> B.Builder
buildAccu (Accu ss) = foldMap B.byteString $ reverse ss

bytesAccu :: Accu -> Bytes
bytesAccu (Accu ss) = B.concat $ reverse ss

-- | Computes a hash value over chromosome names and lengths.  This is
-- useful to fingerprint a reference genome and make sure it is
-- compatible with a given heffalump.  This computes a 64-bit FNV-1 hash
-- and returns the lower 32 bits.

hash_ref :: RefSeqs -> Word32
hash_ref g = fromIntegral . (.&.) 0xFFFFFFFF
           $ hash_list fnv1 (rss_lengths g)
           $ hash_list (B.foldl' fnv1) (rss_chroms g)
           $ (0xdc36d1615b7400a4 :: Word64)
  where
    hash_list  h ar salt = finalise $ foldl' (step h) (salt :!: 0) ar
    step   h (s :!: l) x = h s x :!: succ l
    finalise (s :!: l)   = fnv1 s (l::Int)
    fnv1           h1 h2 = (h1 * 16777619) `xor` fromIntegral h2


-- | Tries to find a genome.  Each pair in the argument is the path to a
-- file whose parent directories should be searched and the name of the
-- file to look for.
findGenome :: [( FilePath, FilePath )] -> IO (Either String RefSeqs)
findGenome files = do
    ddir <- getDataDir
    epth <- (++ [ddir]) . maybe [] splitSearchPath <$> lookupEnv "HEFFALUMP"

    let look = nubHash Hash.empty $ concatMap (look1 epth) files

    filterM doesFileExist look >>= \case
        fp : _ -> Right <$> readTwoBit fp
        [    ] -> return . Left $ "No reference found.  Looked for it at "
                    ++ intercalate ", " look ++ "."
  where
    nubHash _ [    ]                     = []
    nubHash h (x:xs) | x `Hash.member` h = nubHash h xs
                     | otherwise         = x : nubHash (Hash.insert x h) xs

    -- List of names to look at:  the literal name, relative to the
    -- input file; the file name in any directory of the search path;
    -- the file name in any parent directory of the input file.
    look1 epth (hef, rname) =
        makeRelative hef rname :
        map (</> takeFileName rname) (epth ++ parents hef)

    parents f = let d = takeDirectory f in if d == f then [] else d : parents d

